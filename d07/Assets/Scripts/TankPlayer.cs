﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

public class TankPlayer : Tank {

	[Header("HUD Information")]
	public RectTransform canvas;
	public Text hpText;
	public Text missilesText;
	public Image crossCursor;

	public Camera cam = null;

	private Rigidbody rb;
	private bool machineGunCanShot;

	new void Start() {
		base.Start();

		rb = GetComponent<Rigidbody>();
		missilesText.text = realNbMissiles.ToString() + "/" + maxMissiles.ToString();
		machineGunCanShot = true;
	}

	void Update() {

		if (Input.GetKey(KeyCode.LeftShift) && boostTime >= 0f) {
			realMaxSpeed = maxBoostMotorTorque;
		} else {
			boostTime += Time.deltaTime;
			boostTime = Mathf.Clamp(boostTime, 0, maxBoostTime);
			realMaxSpeed = maxMotorTorque;
		}

		if (Input.GetKeyDown(KeyCode.R)) {
			GameManager.instance.RestartScene();
		}
		HandleShooting();
	}

	void FixedUpdate() {
		HandleMoving();
	}

	void LateUpdate() {
		if (cam) {
			cam.transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X"));
			canon.transform.RotateAround(transform.position, transform.up, Input.GetAxis("Mouse X"));

			Vector3 offset = canon.transform.position - (canon.transform.forward * 10f);
			offset.y += 5f;
			cam.transform.position = offset;
			cam.transform.LookAt(canon.transform);

			float angle = Mathf.Abs(Vector3.SignedAngle(transform.forward, canon.transform.forward, Vector3.up));
			canvas.localScale = new Vector3(angle < 90 ? 1 : -1, 1f, 1f);

		}
	}



	void HandleMoving() {
		float motor = realMaxSpeed * Input.GetAxis("Vertical");
		float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

		if (steering != 0 && motor == 0) {
			transform.Rotate(transform.up, steering / 100f);
		}

		foreach (AxleInfo axleInfo in axleInfos) {
			if (axleInfo.steering) {
				axleInfo.leftWheel.steerAngle = steering;
				axleInfo.rightWheel.steerAngle = steering;
			}
			if (axleInfo.motor) {
				axleInfo.leftWheel.motorTorque = motor;
				axleInfo.rightWheel.motorTorque = motor;
			}
		}
	}



	protected override void HandleShooting() {
		if (Input.GetMouseButton(0) && machineGunCanShot) {
			shootSound.Play();
			RaycastHit hit;
			StartCoroutine(MachineGunCoolDown());
			if (Physics.Raycast(canon.transform.position, canon.transform.forward, out hit, 50f)) {
				GameObject explosion = Instantiate(bulletImpact, hit.point, Quaternion.identity);
				Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.duration);

				Debug.DrawRay(canon.transform.position, hit.point - canon.transform.position, Color.red, 5, true);

				if (hit.transform.CompareTag("Tank")) {
					hit.transform.GetComponent<Tank>().TakeDamage(1);
					StartCoroutine(RedCursor());
				}
			}
		}

		if (Input.GetMouseButtonDown(1) && realNbMissiles > 0) {
			shootSound.Play();
			RaycastHit hit;
			if (Physics.Raycast(canon.transform.position, canon.transform.forward, out hit, 50f)) {
				GameObject explosion = Instantiate(canonImpact, hit.point, Quaternion.identity);
				Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.duration);
				realNbMissiles--;
				missilesText.text = realNbMissiles.ToString() + "/" + maxMissiles.ToString();
				rb.AddRelativeForce(new Vector3(0f, 0f, -1000f), ForceMode.Impulse);

				if (hit.transform.CompareTag("Tank")) {
					hit.transform.GetComponent<Tank>().TakeDamage(5);
					StartCoroutine(RedCursor());
				}
			}
		}
	}

	public override void TakeDamage(int amount) {
		base.TakeDamage(amount);
		hpText.text = realHP.ToString();
	}

	IEnumerator RedCursor() {
		crossCursor.color = Color.red;
		yield return new WaitForSeconds(2f);
		crossCursor.color = Color.white;
	}

	IEnumerator MachineGunCoolDown() {
		machineGunCanShot = false;
		yield return new WaitForSeconds(0.2f);
		machineGunCanShot = true;
	}
}

