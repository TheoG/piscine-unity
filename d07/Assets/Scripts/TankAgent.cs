﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class TankAgent : Tank {
	
	private Tank target = null;
	private NavMeshAgent agent;
	private List<Tank> tanks = new List<Tank>();
	private Coroutine shootCoroutine = null;

	public Slider slider;

	new void Start() {
		base.Start();
		agent = GetComponent<NavMeshAgent>();
		tanks.AddRange(FindObjectsOfType<Tank>());
		StartCoroutine(CheckForDistance());
		tanks.Remove(this);
		target = FindClosest();
		agent.destination = target.transform.position;
	}

	private void Update() {
		if (target) {
			if (Vector3.Distance(transform.position, target.transform.position) > range) {
				if (agent.isStopped) {
					agent.isStopped = false;
					agent.ResetPath();
				}
			}
			else {
				canon.transform.LookAt(target.transform.position);
				agent.isStopped = true;
				agent.ResetPath();
				if (shootCoroutine == null) {
					shootCoroutine = StartCoroutine(ShootAtRandomTime());
				}
			}
		}
	}

	private Tank FindClosest() {
		Tank closest = null;
		float distance = float.MaxValue;
		foreach (Tank tank in tanks) {
			if (!tank) continue;
			float tmpDistance = Vector3.Distance(transform.position, tank.transform.position);
			if (tmpDistance < distance) {
				closest = tank;
				distance = tmpDistance;
			}
		}
		return closest;
	}



	protected override void HandleShooting() {
		RaycastHit hit;

		if (Random.Range(0, 2) == 0) {
			if (Physics.Raycast(canon.transform.position, canon.transform.forward, out hit, 50f)) {
				GameObject explosion = Instantiate(bulletImpact, hit.point, Quaternion.identity);
				Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.duration);
				
				Debug.DrawRay(canon.transform.position, hit.point - canon.transform.position, Color.red, 5, true);
				
				if (hit.transform.CompareTag("Tank")) {
					hit.transform.GetComponent<Tank>().TakeDamage(1);
				}
				
			}
		} else {
			if (Physics.Raycast(canon.transform.position, canon.transform.forward, out hit, 50f)) {
				GameObject explosion = Instantiate(canonImpact, hit.point, Quaternion.identity);
				Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.duration);

				Debug.DrawRay(canon.transform.position, hit.point - canon.transform.position, Color.red, 5, true);

				if (hit.transform.CompareTag("Tank")) {
					hit.transform.GetComponent<Tank>().TakeDamage(5);
				}

			}
		}

	}

	public override void TakeDamage(int amount) {
		base.TakeDamage(amount);
		slider.value = ((float)realHP / (float)maxHP);
	}

	IEnumerator CheckForDistance() {
		while (true) {
			yield return new WaitForSeconds(5);
			target = FindClosest();
			agent.destination = target.transform.position;
		}
	}

	IEnumerator ShootAtRandomTime() {
		while (target) {
			yield return new WaitForSeconds(Random.Range(1f, 3f));
			if (target) {
				HandleShooting();	
			}
		}
	}
}
