﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tank : MonoBehaviour {

	public float speed;

	public List<AxleInfo> axleInfos;

	public int maxHP;
	public int maxMissiles;
	public int range;

	public float maxMotorTorque;
	public float maxBoostMotorTorque;
	public float maxBoostTime;

	public float maxSteeringAngle;

	public GameObject canon;
	public GameObject canonImpact;
	public GameObject bulletImpact;

	public AudioSource shootSound;


	protected float realMaxSpeed;
	protected float boostTime;
	protected int realHP;
	protected int realNbMissiles;

	protected void Start () {
		realMaxSpeed = maxMotorTorque;
		boostTime = maxBoostTime;
		realHP = maxHP;
		realNbMissiles = maxMissiles;
	}

	protected abstract void HandleShooting();

	public virtual void TakeDamage(int amount) {
		realHP = realHP - amount;
		if (realHP < 0) {
			realHP = 0;
		}
		if (realHP == 0) {
			Destroy(gameObject);
		}
	}
}

[System.Serializable]
public class AxleInfo {
	public WheelCollider leftWheel;
	public WheelCollider rightWheel;
	public bool motor;
	public bool steering;
}
