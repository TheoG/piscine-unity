﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameObject playerTank;

	public static GameManager instance = null;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	private void Start() {
		Cursor.visible = false;
	}

	private void Update() {
		if (!playerTank) {
			RestartScene();
		}
	}

	public void RestartScene() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
