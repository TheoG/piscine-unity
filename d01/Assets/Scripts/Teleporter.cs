﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D obj) {
		obj.transform.position = transform.GetChild(0).transform.position;
	}
}
