﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	private GameObject toFollow;

	void Update () {
		if (toFollow) {
			if (toFollow.transform.position.y < -4.5f) return;
			float distance = Vector3.Distance(transform.position, toFollow.transform.position);
			transform.position = Vector3.MoveTowards(transform.position, new Vector3(toFollow.transform.position.x, toFollow.transform.position.y, -10.0f), distance * Time.deltaTime);
		}
	}

	public void setToFollow(GameObject obj) {
		toFollow = obj;
	}
}
