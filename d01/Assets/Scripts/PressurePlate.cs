﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour {

	public List<GameObject> gameObjects = new List<GameObject>();

	public ACTION_TYPE action;
	private bool isTriggered = false;

	private List<GameObject> playerOnPlate = new List<GameObject>();

	private Color originalColor;
	private SpriteRenderer spriteRenderer;
	private List<SpriteRenderer> childrenSpriteRenderer = new List<SpriteRenderer>();

	public enum ACTION_TYPE {
		Door,
		Platform
	}

	private void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		originalColor = spriteRenderer.color;
		foreach (Transform child in transform.parent) {
			if (name != child.name) {
				childrenSpriteRenderer.Add(child.GetComponent<SpriteRenderer>());
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		playerOnPlate.Add(collision.gameObject);
		if (playerOnPlate.Count > 1) return;

		transform.Translate(0.0f, -0.015f, 0.0f);
		updateColorEnter(collision.gameObject);
		handleActionEnter(collision.gameObject);
	}

	private void OnTriggerExit2D(Collider2D collision) {
		playerOnPlate.Remove(collision.gameObject);
		handleActionExit(collision.gameObject);
		if (playerOnPlate.Count >= 1) {
			updateColorEnter(playerOnPlate[playerOnPlate.Count - 1]);
			handleActionEnter(playerOnPlate[playerOnPlate.Count - 1]);
		} else {
			updateColorExit();
			transform.Translate(0.0f, 0.015f, 0.0f);
		}
	}

	private void updateColorEnter(GameObject gameObj) {
		Color newColor = getColor(gameObj);

		spriteRenderer.color = newColor;

		foreach (SpriteRenderer child in childrenSpriteRenderer) {
			child.color = newColor;
		}
	}

	private void updateColorExit() {
		spriteRenderer.color = originalColor;
		foreach (SpriteRenderer child in childrenSpriteRenderer) {
			child.color = Color.white;
		}
	}

	private void handleActionEnter(GameObject gameObj) {
		if (action == ACTION_TYPE.Door) {
			if (!isTriggered) {
				foreach(GameObject target in gameObjects) {
					if (target.tag == gameObj.tag) {
						target.transform.Rotate(Vector3.forward, 90f);
						isTriggered = true;
					}
				}
			}
		} else if (action == ACTION_TYPE.Platform) {
			foreach (GameObject target in gameObjects) {
				target.layer = LayerMask.NameToLayer(gameObj.tag.Split('(', ')')[1] + " Platform");
				target.GetComponent<SpriteRenderer>().color = getColor(gameObj);
			}
		}
	}

	private void handleActionExit(GameObject gameObj) {
		if (action == ACTION_TYPE.Door) {
			if (isTriggered) {
				foreach (GameObject target in gameObjects) {
					if (target.tag == gameObj.tag) {
						target.transform.Rotate(Vector3.forward, -90f);
						isTriggered = false;
					}
				}
			}
		}
	}

	private Color getColor(GameObject gameObj) {
		Color newColor = Color.white;
		switch (gameObj.name) {
			case "Thomas":
				newColor = new Color(0.84f, 0.27f, 0.26f);
				break;
			case "Claire":
				newColor = new Color(0.14f, 0.24f, 0.37f);
				break;
			case "John":
				newColor = new Color(0.70f, 0.61f, 0.22f);
				break;
		}
		return newColor;
	}
}
