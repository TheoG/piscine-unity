﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelScript : MonoBehaviour {

	public Dictionary<string, bool> isPlayerDone = new Dictionary<string, bool>();

	void Start () {
		isPlayerDone["John (Yellow)"] = false;
		isPlayerDone["Thomas (Red)"] = false;
		isPlayerDone["Claire (Blue)"] = false;
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.N)) {
			nextLevel();
		}
	}

	public void updatePlayerStatus(string name, bool value) {
		isPlayerDone[name] = value;
	}

	public bool isLevelDone() {
		foreach (KeyValuePair<string, bool> pair in isPlayerDone) {
			if (!pair.Value) {
				return false;
			}
		}
		return true;
	}

	public void nextLevel() {
		int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
		if (SceneManager.sceneCountInBuildSettings > nextSceneIndex) {
			SceneManager.LoadScene(nextSceneIndex);
		}
		else {
			SceneManager.LoadScene(1);
		}
	}

	public void restartLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
