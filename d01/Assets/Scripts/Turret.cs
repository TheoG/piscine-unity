﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

	public enum FireDirection {
		Left = -1,
		Right = 1
	}

	public enum Target {
		Red, 
		Yellow,
		Blue
	}

	[Range(0.1f, 20.0f)]
	public float range;
	[Range(0.1f, 20.0f)]
	public float rateOfFire;
	[Range(0.1f, 10.0f)]
	public float bulletSpeed;
	public FireDirection direction;
	public GameObject bulletPrefab;
	public Target target;

	private float delay = 0.0f;
	private Vector3 canonPosition;
	private Color turretColor;
	private LayerMask targetMask;

	void Start () {
		
		if (direction == FireDirection.Right) {
			transform.Rotate(Vector2.up, 180f);
		}

		canonPosition = new Vector3((float)direction * 0.3f, 0.04f, 0.0f);

		turretColor = Color.white;
		switch (target) {
			case Target.Red:
				turretColor = new Color(0.84f, 0.27f, 0.26f);
				targetMask = LayerMask.NameToLayer("Red Platform");
				break;
			case Target.Yellow:
				turretColor = new Color(0.70f, 0.61f, 0.22f);
				targetMask = LayerMask.NameToLayer("Yellow Platform");
				break;
			case Target.Blue:
				turretColor = new Color(0.14f, 0.24f, 0.37f);
				targetMask = LayerMask.NameToLayer("Blue Platform");
				break;
		}

		foreach (Transform child in transform) {
			if (name != child.name) {
				child.GetComponent<SpriteRenderer>().color = turretColor;
			}
		}
	}

	void Update () {
		delay += Time.deltaTime;
		
		if (delay >= 1.0f / rateOfFire) {
			delay = 0.0f;
			GameObject bullet = Instantiate(bulletPrefab, transform.position + canonPosition, transform.rotation);
			bullet.GetComponent<Rigidbody2D>().velocity = new Vector3((float)direction * bulletSpeed, 0.0f, 0.0f);
			bullet.GetComponent<SpriteRenderer>().color = turretColor;
			bullet.layer = targetMask;
			Destroy(bullet, range);
		}

		if (transform.position.y < -6.0f) {
			Destroy(gameObject);
		}
	}
}
