﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	public Vector3 direction;
	public float distance;
	[Range(0.0f, 20.0f)]
	public float speed;
	private Vector3 originalPosition;
	

	void Start () {
		direction.Normalize();
		originalPosition = transform.position;
	}

	void Update () {
		float oscillation = Mathf.Sin(Time.time * speed) * (distance * 2.0f);
		transform.position = (direction * oscillation) + originalPosition;
	}
}
