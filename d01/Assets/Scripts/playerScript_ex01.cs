﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript_ex01 : MonoBehaviour {

	public float speed;
	public float jumpPower;

	private bool current = false;
	private bool canJump = true;
	private bool grounded = true;
	private bool done = false;
	public PhysicsMaterial2D playerMaterial;

	private Rigidbody2D rigidBody;
	private BoxCollider2D boxCollider2D;
	private CameraScript cameraScript;
	private LevelScript levelScript;
	private GameObject originalParent;

	void Start() {
		cameraScript = GameObject.Find("Main Camera").GetComponent<CameraScript>();
		originalParent = GameObject.Find("Players");
		levelScript = originalParent.GetComponent<LevelScript>();
		
		rigidBody = GetComponent<Rigidbody2D>();
		boxCollider2D = GetComponent<BoxCollider2D>();

		current = CompareTag("John (Yellow)");
		if (current) {
			cameraScript.setToFollow(gameObject);
			setJohnStats();
			boxCollider2D.sharedMaterial = playerMaterial;
		} else {
			boxCollider2D.sharedMaterial = null;
		}

	}

	void Update() {
		if (done) return;

		if (current && Input.GetKey(KeyCode.Space) && grounded && canJump) {
			grounded = false;
			rigidBody.AddForce(new Vector2(0.0f, jumpPower), ForceMode2D.Impulse);
		}

		if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) {
			current = CompareTag("John (Yellow)");
			if (current) {
				cameraScript.setToFollow(gameObject);
				setJohnStats();
				boxCollider2D.sharedMaterial = playerMaterial;
			} else {
				boxCollider2D.sharedMaterial = null;
			}
		} else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) {
			current = CompareTag("Thomas (Red)");
			if (current) {
				cameraScript.setToFollow(gameObject);
				setThomasStats();
				boxCollider2D.sharedMaterial = playerMaterial;
			} else {
				boxCollider2D.sharedMaterial = null;
			}
		} else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) {
			current = CompareTag("Claire (Blue)");
			if (current) {
				cameraScript.setToFollow(gameObject);
				setClaireStats();
				boxCollider2D.sharedMaterial = playerMaterial;
			} else {
				boxCollider2D.sharedMaterial = null;
			}
		}

		if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Backspace)) {
			levelScript.restartLevel();
		}
	}

	void FixedUpdate() {
		if (done) return;
		if (current) {
			if (Input.GetKey(KeyCode.LeftArrow)) {
				rigidBody.velocity = new Vector2(-speed, rigidBody.velocity.y);
			} else if (Input.GetKey(KeyCode.RightArrow)) {
				rigidBody.velocity = new Vector2(speed, rigidBody.velocity.y);
			} else {
				rigidBody.velocity = new Vector2(0.0f, rigidBody.velocity.y);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D obj) {
		if (obj.gameObject.CompareTag("Ground") ||
		    obj.gameObject.CompareTag("John (Yellow)") ||
		    obj.gameObject.CompareTag("Thomas (Red)") ||
		    obj.gameObject.CompareTag("Claire (Blue)") ||
			obj.gameObject.CompareTag("MovingPlatform")) {

			Vector3 hit = obj.contacts[0].normal;
			if (Mathf.Abs(hit.y - 1.0f) < 0.001f) {
				grounded = true;
				rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0.0f);
			}
			if (Mathf.Abs(hit.y - -1.0f) < 0.001f) {
				canJump = false;
			}

			// Set child of the platform only if you're ON it
			if (obj.gameObject.CompareTag("MovingPlatform") && Mathf.Abs(hit.y - 1.0f) < 0.001f) {
				transform.parent = obj.transform;
			}

			if (obj.gameObject.transform.parent && obj.gameObject.transform.parent.CompareTag("MovingPlatform")) {
				transform.parent = obj.gameObject.transform.parent;
			}
		}

		// ex05
		if (obj.gameObject.CompareTag("Bullet") ||
			obj.gameObject.CompareTag("Trap")) {
			Debug.Log("Game Over (" + obj.gameObject.tag +  ")...");
			levelScript.restartLevel();
		}
		
	}

	void OnTriggerEnter2D(Collider2D obj) {
		// ex05
		if (obj.gameObject.CompareTag("Hole")) {
			Debug.Log("Game Over (hole)...");
			//levelScript.restartLevel();
		}
	}


	void OnCollisionExit2D(Collision2D obj) {
		if (obj.gameObject.CompareTag("MovingPlatform")) {
			transform.parent = originalParent.transform;
		}

		if (transform.parent && transform.parent.CompareTag("MovingPlatform")) {
			transform.parent = originalParent.transform;
		}

		canJump = true;
	}

	void OnTriggerStay2D(Collider2D obj) {
		if (CompareTag(obj.tag)) {
			if (Vector3.Distance(transform.position, obj.transform.position) < 0.05f) {
				obj.GetComponent<SpriteRenderer>().color = new Color(0.0f, 1.0f, 0.0f);
				Vector3 snap = obj.transform.position;
				snap.y = transform.position.y;
				transform.position = snap;
				levelScript.updatePlayerStatus(obj.tag, true);
			} else {
				obj.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
				levelScript.updatePlayerStatus(obj.tag, false);
			}

			if (levelScript.isLevelDone()) {
				done = true;
				rigidBody.velocity = new Vector2(0.0f, 0.0f);
				// ex01
				Debug.Log("Level complete!");
				// ex02
				levelScript.nextLevel();
			}
		}
	}

	private void setJohnStats() {
		speed = 1.5f;
		jumpPower = 4.0f;
	}

	private void setThomasStats() {
		speed = 1.0f;
		jumpPower = 3.0f;
	}

	private void setClaireStats() {
		speed = 0.5f;
		jumpPower = 2.75f;
	}
}
