﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class playerScript_ex00 : MonoBehaviour {

	private bool current = false;
	private bool grounded = true;
	private Rigidbody2D rigidBody;
	private CameraScript cameraScript;

	void Start() {
		cameraScript = GameObject.Find("Main Camera").GetComponent<CameraScript>();
		current = CompareTag("John (Yellow)");

		rigidBody = GetComponent<Rigidbody2D>();
	}

	void Update() {

		if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) {
			current = CompareTag("John (Yellow)");
		} else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) {
			current = CompareTag("Thomas (Red)");
		} else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) {
			current = CompareTag("Claire (Blue)");
		}

		if (current) {
			cameraScript.setToFollow(gameObject);
			if (Input.GetKey(KeyCode.LeftArrow)) {
				rigidBody.velocity = new Vector2(-1.0f, rigidBody.velocity.y);
			}
			if (Input.GetKey(KeyCode.RightArrow)) {
				rigidBody.velocity = new Vector2(1.0f, rigidBody.velocity.y);
			}

			if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow)) {
				rigidBody.velocity = new Vector2(0.0f, rigidBody.velocity.y);
			}

			if (Input.GetKey(KeyCode.Space) && grounded) {
				rigidBody.AddForce(new Vector2(0.0f, 3.0f), ForceMode2D.Impulse);
			}
			grounded = Mathf.Abs(rigidBody.velocity.y) < 0.00001f;
		}

		if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Backspace)) {
			SceneManager.LoadScene(0);
		}
	}
}
