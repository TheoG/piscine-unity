﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Building : Entity {

	public delegate void OnBuildingDestroyed(Building building);
	public static event OnBuildingDestroyed onBuildingDestroyed;

	public enum Type {
		TownCenter,
		House,
		Tower,
		Farm,
		Church
	}

	protected Type type;

	new protected void Start() {
		hp = Building.buildingHP[type];
		base.Start();
	}

	override public void decreaseHP(int damage) {
		if (currentHP < 0) return;

		base.decreaseHP(damage);

		if (currentHP <= 0) {
			onBuildingDestroyed(this);
		}
	}

	public Type getType() {
		return type;
	}

	private static Dictionary<Type, int> buildingHP = new Dictionary<Type, int>() {
		{ Type.TownCenter, 10000 },
		{ Type.House, 200 },
		{ Type.Tower, 2000 },
		{ Type.Farm, 100 },
		{ Type.Church, 500 }
	};
}
