﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Town : MonoBehaviour {


	public List<Building> buildings;

	void OnEnable() {
		Building.onBuildingDestroyed += ReduceSpawnTime;
	}

	void OnDisable() {
		Building.onBuildingDestroyed -= ReduceSpawnTime;
	}

	void ReduceSpawnTime(Building building) {
		if (buildings.Contains(building)) {
			foreach(Building b in buildings) {
				if (b && b.getType() == Building.Type.TownCenter) {
					((TownCenter)b).coolDown += 2.5f;
				}
			}
			if (building.getType() == Building.Type.TownCenter) {
				if (building.CompareTag("EnemyBuilding")) {
					Debug.Log("The Human Team wins");
				} else {
					Debug.Log("The Orc Team wins");
				}
			}
		}
	}
}
