﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownCenter : Building {

	public GameObject unitPrefab;

	public float coolDown = 10f;
	private float lastInstantiation = 0.0f;
	public bool spawnEvent = false;
	public Vector3 spawnPoint;


	public delegate void OnPlayerSpawn(Movable newPlayer);
	public static event OnPlayerSpawn onPlayerSpawn;

	public delegate void OnBuildingAttacked(Building building);
	public static event OnBuildingAttacked onBuildingAttacked;

	new void Start () {
		type = Building.Type.TownCenter;
		base.Start();

		if (spawnPoint == Vector3.zero) {
			spawnPoint = transform.position;
		}
	}

	override public void decreaseHP(int damage) {
		base.decreaseHP(damage);

		if (CompareTag("EnemyBuilding")) {
			onBuildingAttacked(this);
		}
	}

	void Update () {

		lastInstantiation += Time.deltaTime;

		if (lastInstantiation >= coolDown) {
			Vector3 randomPositionShift = Vector3.zero;
			randomPositionShift.x = Random.Range(0.01f, 0.7f);
			randomPositionShift.y = Random.Range(0.01f, 0.7f);
			GameObject spawnedPlayer = Instantiate(unitPrefab, spawnPoint + randomPositionShift, Quaternion.identity);
			lastInstantiation = 0.0f;

			if (spawnEvent) {
				onPlayerSpawn(spawnedPlayer.GetComponent<Movable>());
			}
		}

	}
}
