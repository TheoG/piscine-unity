﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance { get; set; }

	public enum Type {
		Voice,
		Fight,
		Destruction
	};

	public AudioSource voiceSoundEffect;
	public AudioSource fightSoundEffect;
	public AudioSource destructionSoundEffect;
	public Dictionary<Type, AudioSource> soundEffects = new Dictionary<Type, AudioSource>();

	void Start () {
		if (!instance) {
			instance = this;
			soundEffects[Type.Voice] = voiceSoundEffect;
			soundEffects[Type.Fight] = fightSoundEffect;
			soundEffects[Type.Destruction] = destructionSoundEffect;
		}
	}

	public void randomizeSoundEffect(Type type, params AudioClip[] clips) {
		if (soundEffects[type] && soundEffects[type].isPlaying) {
			return;
		}

		int index = Random.Range(0, clips.Length);

		soundEffects[type].clip = clips[index];
		soundEffects[type].Play();
	}
}
