﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {

	[SerializeField]
	protected int hp;
	[SerializeField]
	protected int currentHP;

	public AudioClip destruction;
	public int destructionDelay;

	protected void Start () {
		currentHP = hp;
	}
	
	void Update () {
		
	}

	virtual public void decreaseHP(int damage) {
		currentHP -= damage;
		Debug.Log(name + " [" + currentHP + "/" + hp + "]HP has been attacked");

		if (currentHP <= 0) {
			Destroy(gameObject, destructionDelay);
			SoundManager.instance.randomizeSoundEffect(SoundManager.Type.Destruction, destruction);
		}
	}

	public int getHP() {
		return currentHP;
	}
}
