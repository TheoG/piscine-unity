﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	public List<Movable> players;
	
	void Update () {

		if (Input.GetMouseButtonDown(0)) {
			bool addingMode = Input.GetKey(KeyCode.LeftControl);
			Vector3 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			destination.z = 0f;
			RaycastHit2D hit = Physics2D.Raycast(destination, Vector2.zero);

			if (hit && hit.collider.gameObject.CompareTag("Player")) {
				if (!addingMode) {
					setSelectionForAll(false);
				}
				Movable clickedMovable = hit.collider.gameObject.GetComponent<Movable>();
				clickedMovable.setSelected(!clickedMovable.isSelected());

			} else if (hit && (hit.collider.gameObject.CompareTag("Enemy") ||
				hit.collider.gameObject.CompareTag("EnemyBuilding"))) {
				
				foreach (Movable movable in players) {
					if (movable && movable.isSelected()) {
						movable.setNeedSound(true);
						movable.setTarget(hit.collider.gameObject.GetComponent<Entity>());
					}
				}
			} else {
				foreach (Movable movable in players) {
					if (movable && movable.isSelected()) {
						if (movable.getTarget()) {
							movable.setTarget(null);
						} 
						movable.setNeedSound(true);
						movable.setDestinationPoint(destination);
						destination[0] += 0.5f;
					}
				}
			} 
		} else if (Input.GetMouseButtonDown(1)) {
			setSelectionForAll(false);
		} else if (Input.GetMouseButtonDown(2)) {
			setSelectionForAll(true);
		}
	}

	void setSelectionForAll(bool isSelected) {
		foreach (Movable movable in players) {
			if (movable) {
				movable.setSelected(isSelected);
			}
		}
	}

	void OnEnable() {
		TownCenter.onPlayerSpawn += addPlayerToList;
	}

	void OnDisable() {
		TownCenter.onPlayerSpawn -= addPlayerToList;
	}

	void addPlayerToList(Movable m) {
		players.Add(m);
	}
}
