﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : Entity {

	public enum Direction {
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest
	}

	public Direction direction;

	private bool destinationReached = true;
	private Animator animator;
	private Vector3 destinationCoords;
	private Vector3 directionToDestination;
	private bool isLookingRight = true;
	private bool selected = false;
	private SpriteRenderer sprite;
	

	private Entity target;
	public float attackSpeed;
	private float lastAttack;


	private static List<float> angleToDirection = new List<float>(){ 22.5f, 67.5f, 112.5f, 157.5f, 202.5f, 247.5f, 292.5f, 337.5f };


	protected bool needSound = false;
	protected bool needAttackSound = false;
	public float speed;


	new protected void Start () {
		base.Start();
		animator = GetComponent<Animator>();
		sprite = GetComponent<SpriteRenderer>();
		animator.speed = 0;
	}
	
	protected void Update () {
		if (currentHP <= 0) return;
		goTo();
		attack();
	}

	protected void goTo() {
		if (!destinationReached) {
			float distance = Vector3.Distance(transform.position, destinationCoords);
			if (distance < 0.001f) {
				destinationReached = true;
				animator.speed = 0;
				animator.Play(animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, -1, 0f);
			}
			transform.position = Vector3.MoveTowards(transform.position, destinationCoords, speed * Time.deltaTime);
		}
	}

	protected void attack() {
		if (target && !target.CompareTag(tag)) {
			lastAttack += Time.deltaTime;

			float distance = Vector3.Distance(transform.position, target.transform.position);
			if (distance < 0.1f) {
				destinationReached = true;
				if (lastAttack >= attackSpeed) {
					lastAttack = 0f;
					if (target.getHP() > 0) {
						target.decreaseHP(10);
					}
					needAttackSound = true;
					if (!animator.GetBool("IsAttacking")) {
						animator.SetBool("IsAttacking", true);
						animator.speed = 1;
					}
					if (target.getHP() == 0) {
						animator.SetBool("IsAttacking", false);
						animator.Play(animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, -1, 0f);
						animator.speed = 0;
					}
				}
			} else {
				animator.SetBool("IsAttacking", false);
				setDestinationPoint(target.transform.position);
			}
		} else {
			if (animator && animator.GetBool("IsAttacking")) {
				animator.SetBool("IsAttacking", false);
				animator.Play(animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, -1, 0f);
				animator.speed = 0;
			}
		}
	}

	public virtual void setDestinationPoint(Vector3 newDestinationCoords) {
		destinationCoords = newDestinationCoords;
		directionToDestination = destinationCoords - transform.position;
		float angle = Mathf.Atan2(directionToDestination.x, directionToDestination.y) * 180f / Mathf.PI;
		if (angle < 0f) {
			angle = 360f + angle;
		}

		direction = getDirectionFromAngle(angle);
		destinationReached = false;
		int shiftInt = 0;
		animator.speed = 1;
		if (direction > Direction.South) {
			shiftInt = ((int)direction - (int)Direction.South) * 2;
		}
		if ((direction > Direction.South && isLookingRight) || (direction < Direction.South && !isLookingRight)) {
			verticalFlip();
		}
		animator.SetInteger("Direction", (int)direction - shiftInt);

	}

	public void setTarget(Entity target) {
		this.target = target;
		if (!target) {
			animator.SetBool("IsAttacking", false);
		}
	}

	public override void decreaseHP(int damage) {
		base.decreaseHP(damage);
		if (currentHP <= 0) {
			animator.SetBool("IsDying", true);
			animator.speed = 1;
		}
	}

	public Entity getTarget() {
		return target;
	}

	public bool isNeedSound() {
		return needSound;
	}

	public void setNeedSound(bool needSound) {
		this.needSound = needSound;
	}


	void verticalFlip() {
		isLookingRight = !isLookingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	Direction getDirectionFromAngle(float angle) {
		for (int i = 0; i < 8; i++) {
			if (angle < Movable.angleToDirection[i]) {
				return (Direction)i;
			}
		}
		return Direction.North;
	}

	public void setSelected(bool selected) {
		this.selected = selected;
		sprite.color = selected ? Color.green : Color.white;
	}

	public bool isSelected() {
		return selected;
	}
}
