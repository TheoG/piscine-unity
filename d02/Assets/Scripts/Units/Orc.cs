﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orc : Movable {


	// ex04
	public Entity mainTarget;
	public Entity alternativeTarget;

	public AudioClip fight1;
	public AudioClip fight2;

	private float alternativeTargetTime;

	new void Start () {
		hp = 50;
		
		var townCenters = FindObjectsOfType<TownCenter>();
		
		foreach (TownCenter tc in townCenters) {
			if (tc && tc.CompareTag("Player")) {
				mainTarget = tc;
				break;
			}
		}
		base.Start();
	}

	new void Update() {
		RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1.0f, Vector2.zero);

		if (hit && hit.collider.CompareTag("Player")) {
			alternativeTarget = hit.collider.GetComponent<Entity>();
			setTarget(alternativeTarget);
		}

		if (!alternativeTarget || alternativeTarget.getHP() <= 0) {
			setTarget(mainTarget);
		}

		if (alternativeTarget) {
			alternativeTargetTime += Time.deltaTime;
		}

		if (alternativeTargetTime >= 10f) {
			alternativeTarget = null;
			setTarget(mainTarget);
			alternativeTargetTime = 0;
		}

		if (needAttackSound) {
			if (Random.Range(0, 5) == 4) {
				SoundManager.instance.randomizeSoundEffect(SoundManager.Type.Fight, fight1, fight2);
				needAttackSound = false;
			}
		}
		base.Update();
	}


	void OnEnable() {
		TownCenter.onBuildingAttacked += goToTownCenter;
	}

	void OnDisable() {
		TownCenter.onBuildingAttacked -= goToTownCenter;
	}

	void goToTownCenter(Building townCenter) {
		alternativeTarget = townCenter;
		setTarget(alternativeTarget);
	}
}
