﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footman : Movable {

	public AudioClip acknowledge1;
	public AudioClip acknowledge2;
	public AudioClip acknowledge3;
	public AudioClip acknowledge4;

	public AudioClip fight1;
	public AudioClip fight2;

	new void Start () {
		hp = 50;
		base.Start();
	}

	new void Update () {
		base.Update();
		PlaySound();
	}

	public override void setDestinationPoint(Vector3 newDestinationCoords) {
		base.setDestinationPoint(newDestinationCoords);
		PlaySound();
	}

	public void PlaySound() {
		if (needSound) {
			SoundManager.instance.randomizeSoundEffect(SoundManager.Type.Voice, acknowledge1, acknowledge2, acknowledge3, acknowledge4);
			needSound = false;
		}
		if (needAttackSound) {
			if (Random.Range(0, 5) == 4) {
				SoundManager.instance.randomizeSoundEffect(SoundManager.Type.Fight, fight1, fight2);
				needAttackSound = false;
			}
		}
	}
}
