﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	[Range(1f, 20f)]
	public float transitionDuration;

	private Vector3 offset;
	new private Camera camera;
	float h, s, v;

	void Start () {
		offset = Player.player.transform.position - transform.position;
		camera = GetComponent<Camera>();
		s = 1;
	}

	void LateUpdate () {
		if (Player.player) {
			transform.position = Player.player.transform.position - offset;
		}

		Color.RGBToHSV(camera.backgroundColor, out h, out s, out v);
		h += Time.deltaTime / transitionDuration;
		camera.backgroundColor = Color.HSVToRGB(h, s, v);
	}
}
