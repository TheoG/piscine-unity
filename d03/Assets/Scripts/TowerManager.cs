﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour {


	public static TowerManager instance = null;

	private List<GameObject> towers = new List<GameObject>();

	void Start () {
		if (!instance) {
			instance = this;
		}
	}
	
	public void addTower(GameObject tower) {
		towers.Add(tower);
		tower.transform.parent = gameObject.transform;
	}
}
