﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;


public class FireballUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public Sprite rangeSprite;
	public GameObject explosionAnimation;
	public int cost;

	private Image image;
	private CanvasGroup parentCanvasGroup;
	private bool canDrag = true;
	private GameObject draggingIcon;
	private GameObject rangeGameObject;

	public static FireballUI instance = null;

	private float cooldown = 0f;

	void Start() {
		if (!instance) {
			instance = this;
		}
		parentCanvasGroup = GetComponentInParent<CanvasGroup>();
	}

	void Update() {
		cooldown += Time.deltaTime;
		if (CanUse()) {
			parentCanvasGroup.alpha = 1f;
		} else {
			parentCanvasGroup.alpha = 0.2f;
		}
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (cooldown < 10f) {
			canDrag = false;
			return;
		}
		canDrag = true;

		var canvas = GetComponentInParent<Canvas>();
		if (canvas == null)
			return;

		draggingIcon = new GameObject("Icon");
		rangeGameObject = new GameObject("Range");

		draggingIcon.transform.SetParent(canvas.transform, false);
		draggingIcon.transform.SetAsLastSibling();
		draggingIcon.transform.position = transform.position;

		image = draggingIcon.AddComponent<Image>();
		image.sprite = GetComponent<Image>().sprite;
		image.SetNativeSize();

		var spriteRenderer = rangeGameObject.AddComponent<SpriteRenderer>();
		spriteRenderer.sprite = rangeSprite;
		Color newColor = Color.white;
		newColor.a = 0.1f;
		spriteRenderer.color = newColor;
		rangeGameObject.transform.SetParent(draggingIcon.transform);
		rangeGameObject.transform.localPosition = Vector3.zero;

		spriteRenderer.sortingOrder = 1;
		spriteRenderer.sortingLayerName = "ui";
	}


	public void OnDrag(PointerEventData eventData) {
		if (!canDrag) return;
		draggingIcon.transform.position = mouseToWorldTileCoords();
	}


	public void OnEndDrag(PointerEventData eventData) {
		if (!canDrag) return;
		Use();
	}

	private Vector2 mouseToWorldTileCoords() {
		Vector3 pos = Input.mousePosition;
		pos = Camera.main.ScreenToWorldPoint(pos);
		pos.z = 0f;
		return pos;
	}

	public void Use() {
		Instantiate(explosionAnimation, mouseToWorldTileCoords(), Quaternion.identity);

		RaycastHit2D[] hits = Physics2D.CircleCastAll(mouseToWorldTileCoords(), 2.0f, Vector2.zero);
		foreach (RaycastHit2D hit in hits) {
			if (hit && (hit.transform.CompareTag("bot") || hit.transform.CompareTag("flybot") || hit.transform.CompareTag("boss"))) {
				Destroy(hit.transform.gameObject);
			}
		}
		gameManager.gm.playerEnergy -= cost;
		cooldown = 0f;
		Destroy(draggingIcon);
	}



	public bool CanUse() {
		return (cooldown > 10f && gameManager.gm.playerEnergy - cost > 0);
	}
}
