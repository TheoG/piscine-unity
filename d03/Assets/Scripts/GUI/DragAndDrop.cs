﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class DragAndDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public GameObject prefab;
	public Sprite rangeSprite;

	private GameObject draggingIcon;
	private GameObject rangeGameObject;
	private Image image;
	private towerScript tScript;
	private bool canDrag = true;

	private CanvasGroup parentCanvasGroup;

	void Start() {
		parentCanvasGroup = GetComponentInParent<CanvasGroup>();
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (parentCanvasGroup.alpha < 1f || Time.timeScale <= 0.0f) {
			canDrag = false;
			return;
		}
		canDrag = true;
		var canvas = GetComponentInParent<Canvas>();
		if (canvas == null)
			return;

		draggingIcon = new GameObject("Icon");
		rangeGameObject = new GameObject("Range");

		draggingIcon.transform.SetParent(canvas.transform, false);
		draggingIcon.transform.SetAsLastSibling();
		draggingIcon.transform.position = transform.position;

		image = draggingIcon.AddComponent<Image>();
		var recTransform = draggingIcon.GetComponent<RectTransform>();
		image.sprite = GetComponent<Image>().sprite;
		image.SetNativeSize();
		recTransform.localScale = new Vector3(.5f, .5f, 1f);

		var spriteRenderer = rangeGameObject.AddComponent<SpriteRenderer>();
		spriteRenderer.sprite = rangeSprite;
		rangeGameObject.transform.parent = draggingIcon.transform;
		rangeGameObject.transform.localPosition = Vector3.zero;
		tScript = prefab.GetComponent<towerScript>();
		rangeGameObject.transform.localScale *= tScript.range / 2.0f;

		spriteRenderer.sortingOrder = 1;
		spriteRenderer.sortingLayerName = "ui";

		gameManager.gm.playerEnergy -= tScript.energy;
	}

	public void OnDrag(PointerEventData eventData) {
		if (!canDrag) return;
		draggingIcon.transform.position = mouseToWorldTileCoords();

		if (isEmptyTile()) {
			image.color = Color.white;
		} else {
			image.color = Color.red;
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
		if (!canDrag) return;
		if (isEmptyTile()) {
			GameObject instance = Instantiate(prefab, mouseToWorldTileCoords(), Quaternion.identity);
			TowerManager.instance.addTower(instance);
		} else {
			gameManager.gm.playerEnergy += tScript.energy;
		}
		Destroy(draggingIcon);
	}

	private bool isEmptyTile() {
		Vector2 mouseToWorld = mouseToWorldTileCoords();
		RaycastHit2D hit = Physics2D.Raycast(mouseToWorld, Vector2.zero, 0.1f);
		return (hit && hit.transform.CompareTag("empty"));
	}

	private Vector2 mouseToWorldTileCoords() {
		Vector3 pos = Input.mousePosition;
		pos = Camera.main.ScreenToWorldPoint(pos);
		pos.x = Mathf.Round(pos.x);
		pos.y = Mathf.Round(pos.y);
		pos.z = 0f;
		return pos;
	}
}
