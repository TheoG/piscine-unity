﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RadialMenuUI : MonoBehaviour {

	[Header("Radial Menu")]
	public CanvasRenderer radialMenuCanvasRenderer;

	[Header("Prices")]
	public Text upgradePrice;
	public Text downgradeValue;

	[Header("UI Element")]
	public CanvasGroup upgradeCanvas;

	[Header("Downgrade / Sell images")]
	public Image rangeImage;

	[Header("Downgrade / Sell images and sprites")]
	public Sprite downgradeSprite;
	public Sprite sellSprite;
	public Image downgradeSellImage;

	private GameObject currentSelectedTower = null;
	private towerScript currentScript = null;
	private towerScript parentScript = null;

	void Update() {
		HandleRightClick();
		HandleLeftClick();
		if (currentSelectedTower && currentScript) {
			
			radialMenuCanvasRenderer.transform.GetChild(1).gameObject.SetActive(currentScript.upgrade);
			if (parentScript) {
				upgradePrice.text = parentScript.energy.ToString();
			}
			downgradeValue.text = (currentScript.energy / 2).ToString();

			if (gameManager.gm.playerEnergy < parentScript.energy) {
				upgradeCanvas.alpha = 0.5f;
				upgradeCanvas.interactable = false;
				upgradeCanvas.blocksRaycasts = false;
			} else {
				upgradeCanvas.alpha = 1f;
				upgradeCanvas.interactable = true;
				upgradeCanvas.blocksRaycasts = true;
			}
		}

		if (Time.timeScale <= 0f) {
			ClosePanel();
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			ClosePanel();
		}
	}

	private void HandleLeftClick() {
		if (Input.GetMouseButtonDown(0)) {
			Vector3 mouseWorldCoords = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (currentSelectedTower) {
				float distance = Vector3.Distance(mouseWorldCoords, currentSelectedTower.transform.position);

				if (distance > 10.6f) {
					ClosePanel();
				}
			}
		}
	}

	private void HandleRightClick() {
		if (Input.GetMouseButtonDown(1)) {

			Vector3 mouseWorldCoords = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (currentSelectedTower) {
				float distance = Vector3.Distance(mouseWorldCoords, currentSelectedTower.transform.position);
				if (distance > 5f) {
					ClosePanel();
				}
			}

			RaycastHit2D[] hits = Physics2D.RaycastAll(mouseWorldCoords, Vector2.zero);
			foreach (RaycastHit2D hit in hits) {
				if (hit && hit.transform.CompareTag("tower")) {
					radialMenuCanvasRenderer.transform.position = hit.transform.position;
					radialMenuCanvasRenderer.transform.gameObject.SetActive(true);
					currentSelectedTower = hit.transform.gameObject;

					currentScript = currentSelectedTower.GetComponent<towerScript>();
					parentScript = currentScript.upgrade.GetComponent<towerScript>();
					UpdateIcons();
					return;
				}
			}
		}
	}

	public void UpgradeTower() {
		if (currentSelectedTower) {
			Transform turretTransform = currentSelectedTower.transform;
			GameObject instance = Instantiate(currentScript.upgrade);
			instance.transform.position = currentSelectedTower.transform.position;
			TowerManager.instance.addTower(instance);
			Destroy(currentSelectedTower);
			gameManager.gm.playerEnergy -= parentScript.energy;

			currentSelectedTower = instance;
			currentScript = currentScript.upgrade.GetComponent<towerScript>();
			if (currentScript.upgrade) {
				parentScript = currentScript.upgrade.GetComponent<towerScript>();
			}
			UpdateIcons();
		}
	}

	public void SellTower() {
		if (currentSelectedTower) {

			if (currentScript.downgrade) {
				Transform turretTransform = currentSelectedTower.transform;
				GameObject instance = Instantiate(currentScript.downgrade);
				instance.transform.position = currentSelectedTower.transform.position;
				TowerManager.instance.addTower(instance);
				Destroy(currentSelectedTower);
				currentSelectedTower = instance;

				parentScript = currentScript;
				currentScript = currentScript.downgrade.GetComponent<towerScript>();
			} else {
				Destroy(currentSelectedTower);
				ClosePanel();
			}
			gameManager.gm.playerEnergy += currentScript.energy / 2;
			UpdateIcons();
		}
	}

	private void UpdateIcons() {
		float r = currentScript.range / 2;
		rangeImage.transform.localScale = new Vector3(r, r, 1f);
		if (currentScript.downgrade) {
			downgradeSellImage.sprite = downgradeSprite;
			downgradeSellImage.GetComponent<RectTransform>().rotation = new Quaternion(0f, 0f, 180f, 1f);
		} else {
			downgradeSellImage.sprite = sellSprite;
			downgradeSellImage.GetComponent<RectTransform>().rotation = new Quaternion(0f, 0f, 0f, 1f);
		}
	}

	public void ClosePanel() {
		radialMenuCanvasRenderer.transform.gameObject.SetActive(false);
		currentSelectedTower = null;
	}
}
