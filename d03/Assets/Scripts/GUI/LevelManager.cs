﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance;

	public Texture2D cursorTexture;

	void Start() {
		if (!instance) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		ResetCursor();
	}

	public void StartLevel(int sceneIndex) {
		SceneManager.LoadScene(sceneIndex);
	}

	public void StartNextLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void RestartLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public bool IsLastLevel() {
		return SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings - 1;
	}

	public void ResetCursor() {
		Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
	}

	public void Quit() {
		Application.Quit();
	}
}
