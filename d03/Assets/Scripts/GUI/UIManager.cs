﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour {

	[Header("GUI info")]
	public Text playerEnergy;
	public Text playerHP;
	public Text waveNumber;
	public Text speedText;

	[Header("End Game Panel")]
	public Text endGameStatus;
	public Text scoreText;
	public Text rankText;
	public Button endGameButton;
	public Text endLevelButtonText;

	[Header("Tower Icons Shortcuts")]
	public GameObject canon1;
	public GameObject gatlin1;
	public GameObject rocket1;
	public Texture2D canon1Cursor;
	public Texture2D gatlin1Cursor;
	public Texture2D rocket1Cursor;
	public Sprite rangeSprite;

	private bool isPaused = false;
	private bool isEnd = false;
	private List<KeyValuePair<string, int>> rankingList = new List<KeyValuePair<string, int>>();
	private bool shortcutTower = false;
	private GameObject rangeTower = null;
	private GameObject futurInstance = null;
	private towerScript futurInstanceScript = null;

	void Start() {
		InitRankingList();
		rangeTower = new GameObject("RangeTower");
		SpriteRenderer spriteRenderer = rangeTower.AddComponent<SpriteRenderer>();
		spriteRenderer.sortingOrder = 1;
		spriteRenderer.sortingLayerName = "ui";
		spriteRenderer.sprite = rangeSprite;
		rangeTower.SetActive(false);
	}

	void Update () {

		if (!isEnd && Input.GetKeyDown(KeyCode.Escape)) {
			DisplayPauseMenu();
		}

		if (shortcutTower && !futurInstance) {
			FireballUI.instance.OnDrag(null);
		}

		UpdatePlayerData();
		HandleLeftClick();
		HandleKeyboardShortcuts();
	}

	void HandleLeftClick() {
		if (Input.GetMouseButtonDown(0)) {
			if (shortcutTower && futurInstance) {
				LevelManager.instance.ResetCursor();
				shortcutTower = false;
				rangeTower.SetActive(false);
				if (isEmptyTile() && gameManager.gm.playerEnergy - futurInstanceScript.energy > 0) {
					GameObject instance = Instantiate(futurInstance);
					instance.transform.position = mouseToWorldTileCoords();
					TowerManager.instance.addTower(instance);
					gameManager.gm.playerEnergy -= futurInstanceScript.energy;
				}
			} else if (shortcutTower && !futurInstance) {
				FireballUI.instance.Use();
				Cursor.visible = true;
				shortcutTower = false;
			}
		}
	}

	void HandleKeyboardShortcuts() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			shortcutTower = false;
			LevelManager.instance.ResetCursor();
		}
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			Vector2 hotSpot = new Vector2(canon1Cursor.width / 2f, canon1Cursor.height / 2f);
			Cursor.SetCursor(canon1Cursor, hotSpot, CursorMode.Auto);
			futurInstance = canon1;
			shortcutTower = true;
			rangeTower.SetActive(true);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			Vector2 hotSpot = new Vector2(canon1Cursor.width / 2f, canon1Cursor.height / 2f);
			Cursor.SetCursor(gatlin1Cursor, hotSpot, CursorMode.Auto);
			futurInstance = gatlin1;
			shortcutTower = true;
			rangeTower.SetActive(true);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			Vector2 hotSpot = new Vector2(canon1Cursor.width / 2f, canon1Cursor.height / 2f);
			Cursor.SetCursor(rocket1Cursor, hotSpot, CursorMode.Auto);
			futurInstance = rocket1;
			shortcutTower = true;
			rangeTower.SetActive(true);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha4)) {
			if (FireballUI.instance.CanUse()) {
				Cursor.visible = false;
				futurInstance = null;
				shortcutTower = true;
				rangeTower.SetActive(false);
				FireballUI.instance.OnBeginDrag(null);
			}
		}

		if (shortcutTower && futurInstance) {
			Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			pos.z = 0;
			rangeTower.transform.position = pos;
			futurInstanceScript = futurInstance.GetComponent<towerScript>();
			float r = futurInstanceScript.range / 2;
			rangeTower.transform.localScale = new Vector3(r, r, 1f);
		}
	}

	void UpdatePlayerData() {
		playerHP.text = gameManager.gm.playerHp.ToString();
		playerEnergy.text = gameManager.gm.playerEnergy.ToString();
		waveNumber.text = gameManager.gm.currentWave.ToString() + " / " + gameManager.gm.totalWavesNumber;
	}

	public void DisplayPauseMenu() {
		isPaused = !isPaused;
		gameManager.gm.pause(isPaused);

		Transform pauseMenu = transform.GetChild(1);
		pauseMenu.gameObject.SetActive(isPaused);
		rangeTower.SetActive(false);
	}

	public void DisplayConfirmationMenu(bool display) {
		Transform confirmationMenu = transform.GetChild(2);
		confirmationMenu.gameObject.SetActive(display);
	}

	public void DisplayEndLevelMenu(bool win) {
		if (isEnd) return;
		isEnd = true;
		Transform endMenu = transform.GetChild(3);
		endMenu.gameObject.SetActive(true);
		gameManager.gm.pause(true);
		endGameStatus.text = win ? "Victory!" : "Game Over...";
		scoreText.text += gameManager.gm.score;
		rankText.text += GetRanking();
		endLevelButtonText.text = win ? "Next Level" : "Retry?";
		if (win) {
			if (LevelManager.instance.IsLastLevel()) {
				endGameButton.gameObject.SetActive(false);
			} else {
				endGameButton.onClick.AddListener(LevelManager.instance.StartNextLevel);
			}
		} else {
			endGameButton.onClick.AddListener(LevelManager.instance.RestartLevel);
		}
	}

	public void Confirm(bool confirmation) {
		if (confirmation) {
			LevelManager.instance.StartLevel(0);
		} else {
			DisplayPauseMenu();
			DisplayConfirmationMenu(false);
		}
	}

	public void SetSpeed(int speed) {
		gameManager.gm.changeSpeed(speed);
		if (speed > 0) {
			speedText.text = "Speed x" + speed.ToString();
		} else {
			speedText.text = "Paused";
		}
	}

	private string GetRanking() {
		foreach (KeyValuePair<string, int> rank in rankingList) {
			if (rank.Value > gameManager.gm.score + gameManager.gm.playerEnergy * 2) {
				return rank.Key;
			}
		}
		return "F";
	}

	private void OnEnable() {
		ennemyScript.onLevelEnd += DisplayEndLevelMenu;
		gameManager.onLevelEnd += DisplayEndLevelMenu;
	}

	private void OnDisable() {
		ennemyScript.onLevelEnd -= DisplayEndLevelMenu;
		gameManager.onLevelEnd -= DisplayEndLevelMenu;
	}

	private void InitRankingList() {
		rankingList.Add(new KeyValuePair<string, int>("F", 100));
		rankingList.Add(new KeyValuePair<string, int>("E", 1000));
		rankingList.Add(new KeyValuePair<string, int>("D", 5000));
		rankingList.Add(new KeyValuePair<string, int>("C", 10000));
		rankingList.Add(new KeyValuePair<string, int>("B", 50000));
		rankingList.Add(new KeyValuePair<string, int>("A", 100000));
		rankingList.Add(new KeyValuePair<string, int>("S", 500000));
		rankingList.Add(new KeyValuePair<string, int>("SS", 1000000));
		rankingList.Add(new KeyValuePair<string, int>("SSS", 50000000));
	}

	private Vector2 mouseToWorldTileCoords() {
		Vector3 pos = Input.mousePosition;
		pos = Camera.main.ScreenToWorldPoint(pos);
		pos.x = Mathf.Round(pos.x);
		pos.y = Mathf.Round(pos.y);
		pos.z = 0f;
		return pos;
	}

	private bool isEmptyTile() {
		Vector2 mouseToWorld = mouseToWorldTileCoords();
		RaycastHit2D hit = Physics2D.Raycast(mouseToWorld, Vector2.zero, 0.1f);
		return (hit && hit.transform.CompareTag("empty"));
	}
}
