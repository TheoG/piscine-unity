﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TowerUI : MonoBehaviour {

	public GameObject tower;
	public Sprite fly;
	public Sprite noFly;

	private towerScript towerCaracteristics;
	private CanvasGroup canvasGroup;

	void Start () {
		towerCaracteristics = tower.GetComponent<towerScript>();
		canvasGroup = GetComponent<CanvasGroup>();

		Text[] texts = GetComponentsInChildren<Text>();
		Image[] images = GetComponentsInChildren<Image>();

		if (texts.Length == 4) {
			texts[0].text = towerCaracteristics.fireRate.ToString();
			texts[1].text = towerCaracteristics.damage.ToString();
			texts[2].text = towerCaracteristics.range.ToString();
			texts[3].text = towerCaracteristics.energy.ToString();
			images[3].sprite = towerCaracteristics.type == towerScript.Type.canon ? noFly : fly;
		}
	}
	
	void Update () {
		canvasGroup.alpha = gameManager.gm.playerEnergy - towerCaracteristics.energy < 0 ? 0.2f : 1f;
	}
}
