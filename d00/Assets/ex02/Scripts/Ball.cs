﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public float clubPower = 0.0f;
	public float ballPower = 0.0f;
	public bool isShooting = false;
	public bool firstSpace = true;
	public GameObject club;

	private int score = -15;
	private short direction = 1;
	private bool endGame = false;

	void Update() {
		if (endGame) return;
		if (Input.GetKey(KeyCode.Space) && !isShooting) {
			if (firstSpace) {
				firstSpace = false;
				ballPower = 0.0f;
				clubPower = 0.0f;
			}

			if (clubPower < 2.0f) {
				clubPower += 1.0f * Time.deltaTime;
				ballPower += 10.0f * Time.deltaTime;
			}
		} else {
			firstSpace = true;
			if (clubPower > 0.0f) {
				clubPower -= 6.0f * Time.deltaTime;
			}
			if (clubPower <= 0.0f && ballPower > 0.0f) {
				isShooting = true;
			}
			if (isShooting) {
				if (transform.position.y > 3.7f) {
					direction = -1;
				} else if (transform.position.y < -3.6f) {
					direction = 1;
				}
				transform.Translate(0.0f, ballPower * Time.deltaTime * direction, 0.0f);
				ballPower -= 5f * Time.deltaTime;

				CheckEndGame();

				if (ballPower < 0.0f) {
					ballPower = 0.0f;
					isShooting = false;
					score += 5;
					Debug.Log("Score: " + score);
					if (club) {
						Vector3 newPos = Vector3.zero;
						newPos.x = -0.2f;
						newPos.y = transform.position.y + 0.2f;
						club.transform.position = newPos;
					}
					if (transform.position.y > 3.0f) {
						Quaternion clubRotation = club.transform.rotation;
						clubRotation.z = 180;
						club.transform.rotation = clubRotation;
					} else if (transform.position.y < 3.0f) {
						Quaternion clubRotation = club.transform.rotation;
						clubRotation.z = 0;
						club.transform.rotation = clubRotation;
					}
				}
			} else {
				if (transform.position.y > 3.0f) {
					direction = -1;
				} else if (transform.position.y < 3.0f) {
					direction = 1;
				}
			}
		}
	}

	void CheckEndGame() {
		if (transform.position.y > 2.8f && transform.position.y < 3.2f && ballPower < 0.2f) {
			gameObject.transform.Translate(0f, 0f, -20f);
			if (score > 0) {
				Debug.Log("You lose...");
			} else {
				Debug.Log("You win !");
			}
			endGame = true;
		}
	}
}
