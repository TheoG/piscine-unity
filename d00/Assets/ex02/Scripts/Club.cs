﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Club : MonoBehaviour {

	public float power = 0.0f;
	public float originalPosition = 0.0f;
	public bool firstSpace = true;

	private bool isShooting = false;
	private short direction = 1;

	void Update () {
		if (originalPosition > 3.0f) {
			direction = -1;
		} else if (originalPosition < 3.0f) {
			direction = 1;
		}
		if (Input.GetKey(KeyCode.Space) && !isShooting) {
			if (firstSpace) {
				firstSpace = false;
				power = 0.0f;
				originalPosition = transform.position.y;
			}
			if (power < 2.0f) {
				power += 1.0f * Time.deltaTime;
				Vector3 newPos = Vector3.zero;
				newPos.x = -0.2f;
				newPos.y = originalPosition - power * direction;
				transform.position = newPos;
			}
		} else {
			if (power > 0.0f) {
				isShooting = true;
			} else {
				firstSpace = true;
			}
			if (isShooting && power >= 0.0f) {
				power -= 6.0f * Time.deltaTime;
				Vector3 newPos = Vector3.zero;
				newPos.x = -0.2f;
				newPos.y = originalPosition - power * direction;
				transform.position = newPos;
				if (power < 0.0f) {
					isShooting = false;
					power = 0.0f;
				}
			}
		}
	}
}
