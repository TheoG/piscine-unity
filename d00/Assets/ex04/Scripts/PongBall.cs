﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBall : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;

	public int direction = 1;
	float yValue = 0.0f;

	public int scorePlayer1 = 0;
	public int scorePlayer2 = 0;

	public float coolDown = 0.0f;


	void Start () {
		RandomiseValues();
	}
	
	void Update () {

		if (coolDown > 0.0f) {
			coolDown -= Time.deltaTime;
			return;
		}

		if (direction == -1) {
			HandleCollisionPlayer1();
		} else {
			HandleCollisionPlayer2();
		}
		HandleBorderCollisions();
		transform.Translate(direction * Time.deltaTime * 6.0f, yValue * Time.deltaTime, 0.0f);
	}

	void HandleCollisionPlayer1() {
		if (transform.position.x <= -8.0f) {
			if (transform.position.y - 0.25f < player1.transform.position.y + 1.0f
			    && transform.position.y + 0.25f > player1.transform.position.y - 1.0f) {
				direction = -direction;
			}
		}
	}

	void HandleCollisionPlayer2() {
		if (transform.position.x >= 8.0f) {
			if (transform.position.y - 0.25f < player2.transform.position.y + 1.0f
				&& transform.position.y + 0.25f > player2.transform.position.y - 1.0f) {
				direction = -direction;
			}
		}
	}

	void RandomiseValues() {
		direction = 1;
		if (Random.Range(0, 2) == 1) {
			direction = -1;
		}
		if (Random.Range(0, 2) == 0) {
			yValue = Random.Range(-0.5f, -3.0f);
		} else {
			yValue = Random.Range(0.5f, 3.0f);
		}
		transform.Translate(-transform.position.x + 0.1f, -transform.position.y, 0.0f);
	}


	void HandleBorderCollisions() {
		if (yValue > 0.0f && transform.position.y >= 4.65f || yValue < 0.0f && transform.position.y <= -4.65f) {
			yValue = -yValue;
		}

		if (transform.position.x >= 9.0f) {
			scorePlayer1++;
			Debug.Log("Player 1: " + scorePlayer1 + " | Player 2: " + scorePlayer2);
			RandomiseValues();
			coolDown = 1.0f;
		}
		if (transform.position.x <= -9.0f) {
			scorePlayer2++;
			Debug.Log("Player 1: " + scorePlayer1 + " | Player 2: " + scorePlayer2);
			RandomiseValues();
			coolDown = 1.0f;
		}
	}
}
