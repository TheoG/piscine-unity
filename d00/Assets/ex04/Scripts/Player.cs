﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;

	void Update () {
		HandlePlayer1();
		HandlePlayer2();
	}


	void HandlePlayer1() {
		if (Input.GetKey(KeyCode.W) && player1.transform.position.y < 3.5f) {
			player1.transform.Translate(0.0f, 6.0f * Time.deltaTime, 0.0f);
		} else if (Input.GetKey(KeyCode.S) && player1.transform.position.y > -3.5f) {
			player1.transform.Translate(0.0f, -6.0f * Time.deltaTime, 0.0f);
		}
	}

	void HandlePlayer2() {
		if (Input.GetKey(KeyCode.UpArrow) && player2.transform.position.y < 3.5f) {
			player2.transform.Translate(0.0f, 6.0f * Time.deltaTime, 0.0f);
		} else if (Input.GetKey(KeyCode.DownArrow) && player2.transform.position.y > -3.5f) {
			player2.transform.Translate(0.0f, -6.0f * Time.deltaTime, 0.0f);
		}
	}
}
