﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour {

	private const float defaultBreath = 2.0f;
	private float breath;

	void Start () {
		breath = defaultBreath;
	}

	void Update () {

		Vector3 newScale = transform.localScale;

		if (Input.GetKeyDown(KeyCode.Space)) {
			if (breath >= 0.0f) {
				newScale.x += 0.2f;
				newScale.y += 0.2f;
				newScale.z += 0.2f;

				transform.localScale = newScale;
				breath -= 0.5f;
			}
		} else if (breath <= defaultBreath) {
			breath += 0.025f;
		}

		newScale.x -= 0.0075f;
		newScale.y -= 0.0075f;
		newScale.z -= 0.0075f;

		transform.localScale = newScale;

		if (transform.localScale.x >= 3.0f || transform.localScale.x <= 0.0f) {
			Debug.Log("Balloon life time: " + Mathf.RoundToInt(Time.realtimeSinceStartup) + "s");
			Destroy(gameObject);
		}
	}
}
