﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

	public GameObject bird;
	public float speed = 0.1f;
	float realTime = 0.0f;

	private bool lose = false;
	private bool gameStarted = false;
	private int score = 0;

	void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			if (!gameStarted) {
				gameStarted = true;
			}
		}
		if (lose || !gameStarted) return;

		realTime += Time.deltaTime;
		transform.Translate(-speed * Time.deltaTime, 0.0f, 0.0f);

		if (transform.position.x < -3.0f) {
			speed += 0.075f;
			transform.Translate(6.0f, 0.0f, 0.0f);
			score += 5;
		}

		if (bird && bird.transform.position.y <= -2.2f) {
			lose = true;
			displayScore();
		}

		if (bird.transform.position.x + 0.5f > transform.position.x - 0.5f && bird.transform.position.x < transform.position.x + 1.0) {
			if (bird.transform.position.y > 1.4f || bird.transform.position.y < -0.7f) {
				lose = true;
				displayScore();
			}
		}
	}

	void displayScore() {
		Debug.Log("Score: " + score);
		Debug.Log("Time: " + Mathf.RoundToInt(realTime) + "s");
	}
}
