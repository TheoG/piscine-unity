﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

	public GameObject pipe;
	public float force = -0.05f;
	public float addefForce = 5.0f;

	private bool gameStarted = false;
	private bool lose = false;


	void Update () {

		if (lose) return;

		if (Input.GetKeyDown(KeyCode.Space)) {
			if (!gameStarted) {
				gameStarted = true;
			} else {
				force += addefForce * Time.deltaTime;
			}
		}

		if (gameStarted) {
			force -= 0.2f * Time.deltaTime;
			transform.Rotate(Vector3.forward, force * 15.0f, Space.World);
			transform.Translate(0.0f, force, 0.0f, Space.World);
		}

		lose |= transform.position.y <= -2.2f;

		if (transform.position.x + 0.5f > pipe.transform.position.x - 0.5f && transform.position.x < pipe.transform.position.x + 1.0) {
			if (transform.position.y > 1.4f || transform.position.y < -0.7f) {
				lose = true;
			}
		}
	}
}
