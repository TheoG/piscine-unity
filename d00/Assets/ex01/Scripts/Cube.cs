﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

	public float speed = -0.01f;
	public KeyCode code;

	void Start () {
		speed = Random.Range(-0.01f, -0.05f);
	}
	
	void Update () {
		transform.Translate(0.0f, speed, 0.0f);
		if (transform.position.y <= -3.0f) {
			Destroy(gameObject);
		}

		if (Input.GetKeyDown(code)) {
			CheckPrecision();
		}
	}

	private void CheckPrecision() {
		if (transform.position.y <= 0.0f) {
			float distance = -1.0f - transform.position.y;
			if (distance < 0.0f) {
				distance = -distance;
			}
			Debug.Log("Precision: " + distance);
			Destroy(gameObject);
		}
	}
}