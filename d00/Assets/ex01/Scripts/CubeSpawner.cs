﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour {

	private float timer = 4.0f;

	public GameObject cubeAPrefab;
	public GameObject cubeSPrefab;
	public GameObject cubeDPrefab;

	private GameObject currentCubeA;
	private GameObject currentCubeS;
	private GameObject currentCubeD;

	private float deltaTimeA = 0.0f;
	private float deltaTimeS = 0.0f;
	private float deltaTimeD = 0.0f;
	
	void Update () {

		timer += Time.deltaTime;
		if (timer >= 4.0f) {
			timer = 0.0f;

			deltaTimeA = Random.Range(0.3f, 1.5f);
			deltaTimeS = Random.Range(0.3f, 1.5f);
			deltaTimeD = Random.Range(0.3f, 1.5f);
		}

		if (timer > deltaTimeA && deltaTimeA > 0.0f && !currentCubeA) {
			if (Random.Range(0, 1) == 0) {
				currentCubeA = Instantiate(cubeAPrefab);
				deltaTimeA = 0.0f;
			}
		}
		if (timer > deltaTimeS && deltaTimeS > 0.0f && !currentCubeS) {
			if (Random.Range(0, 1) == 0) {
				currentCubeS = Instantiate(cubeSPrefab);
				deltaTimeS = 0.0f;
			}
		}
		if (timer > deltaTimeD && deltaTimeD > 0.0f && !currentCubeD) {
			if (Random.Range(0, 1) == 0) {
				currentCubeD = Instantiate(cubeDPrefab);
				deltaTimeD = 0.0f;
			}
		}
	}
}
