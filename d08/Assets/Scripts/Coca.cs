﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coca : MonoBehaviour {

	public float speed;

	void Update () {
		transform.Rotate(Vector3.up, Time.deltaTime * speed, Space.World);
	}

	private void OnTriggerEnter(Collider other) {
		Player.instance.Heal();
		Destroy(gameObject);
	}
}
