﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class ZombieAI : MonoBehaviour {


	private NavMeshAgent agent;
	private Animator animator;

	private Transform target;
	private Zombie zombie;

	private void Start() {
		agent = transform.parent.GetComponent<NavMeshAgent>();
		animator = transform.parent.GetComponent<Animator>();
		zombie = transform.parent.GetComponent<Zombie>();
		agent.stoppingDistance = 1f;
	}

	void Update() {
		if (zombie.GetHP() <= 0) return;
		if (target) {
			agent.destination = target.position;
			if (agent.remainingDistance <= 1.1f) {
				animator.SetBool("IsRunning", false);
				animator.SetBool("IsAttacking", true);
			} else {
				if (!animator.GetBool("IsRunning")) {
					animator.SetBool("IsRunning", true);
					animator.SetBool("IsAttacking", false);
				}
			}
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player") && animator) {

			animator.SetBool("IsRunning", true);
			agent.destination = other.transform.position;
			target = other.transform;

		}
	}

	private void OnTriggerStay(Collider other) {
		if (animator && other.CompareTag("Player") && agent.destination == agent.transform.position) {
			agent.destination = other.transform.position;
			target = other.transform;
		}
	}
}
