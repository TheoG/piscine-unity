﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Zombie : MonoBehaviour {

	public delegate void OnZombieDie();
	public event OnZombieDie onZombieDie;

	public int maxHP;
	public bool isDying = false;

	public GameObject loot;

	[SerializeField]
	private int realHP;
	private Animator animator;
	private NavMeshAgent agent;
	private Stats stats;

	void Start () {
		animator = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		stats = GetComponent<Stats>();
		realHP = stats.HP;
	}
	
	public bool TakeDamage(int amount) {
		realHP -= amount;
		if (realHP <= 0) {
			animator.SetBool("IsDead", true);
			agent.destination = transform.position;
			agent.ResetPath();
			return false;
		}
		return true;
	}

	public void DieOnGround() {
		isDying = true;
		agent.enabled = false;
		StartCoroutine(GoBackToGround());
	}

	IEnumerator GoBackToGround() {
		yield return new WaitForSeconds(2f);
		GetComponent<Collider>().enabled = false;
		Vector3 oldPosition = transform.position;
		oldPosition.y += 0.5f;
		if (Random.Range(0, 10) < 1) {
			Instantiate(loot, oldPosition, loot.transform.rotation);
		}
		for (int i = 0; i < 100; i++) {
			transform.Translate(0f, -0.01f, 0f);
			yield return new WaitForSeconds(0.01f);
		}
		Destroy(gameObject);
		onZombieDie();
	}

	public void HitPlayer() {
		Stats targetStats = Player.instance.GetStats();
		int chance = stats.HitChance(targetStats);

		if (Random.Range(0, 101) < chance && !Player.instance.TakeDamage(stats.DamageValue(targetStats))) {
			agent.destination = transform.position;
		}
	}

	public void SetLevel(int level) {

		stats = GetComponent<Stats>();

		stats.strengh = (int)stats.strengh + (int)((float)stats.strengh * (0.15f * level));
		stats.agility = (int)stats.agility + (int)((float)stats.agility * (0.15f * level));
		stats.constitution = (int)stats.constitution + (int)((float)stats.constitution * (0.15f * level));
		stats.level = level;
		stats.ComputeCaracteristics();
	}


	public int GetHP() {
		return realHP;
	}

	public Stats GetStats() {
		return stats;
	}
}
