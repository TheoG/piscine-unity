﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine;

public class Player : MonoBehaviour {

	public AnimationClip animationClip;
	public Animation m_animation;

	[Header("Player Panel")]
	public Slider hpSlider;
	public Slider xpSlider;
	public Text hpText;
	public Text xpText;
	public Text lvlText;
	public GameObject gameOverPanel;
	public GameObject newPointPanel;

	[Header("Enemy Panel")]
	public GameObject enemyPanel;
	public Slider enemyHP;
	public Text enemyNameAndLvl;
	private Stats enemyStats;

	public static Player instance = null;

	public ParticleSystem ptcSystem;
	public int requiredXP;

	private NavMeshAgent agent;
	private Camera cam;
	private Animator animator;
	private Stats stats;

	private float camDistance = 10f;
	private Zombie target = null;
	private bool targetLocked = false;
	private bool targetReleased = false;
	private bool lastAttack = false;
	private bool gameOver = false;
	private float realHP;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	void Start () {
		agent = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator>();
		stats = GetComponent<Stats>();

		realHP = stats.HP;

		cam = FindObjectOfType<Camera>();

		GameObject sword = transform.GetChild(0).gameObject;

		sword.transform.SetParent(animator.GetBoneTransform(HumanBodyBones.RightHand));
		sword.transform.localPosition = Vector3.zero;
		sword.transform.Translate(Vector3.forward / 2);
	}
	
	void Update () {
		HandleGameOver();
		if (gameOver) return;
		if (!Caracteristics.instance.visible) {
			HandleMouseClick();
			HandleMousePosition();
		}
		UpdatePlayerInfo();


		if (target) {
			agent.destination = target.transform.position;
		}

		if (agent.remainingDistance <= agent.stoppingDistance) {
			animator.SetBool("IsRunning", false);
			if (target) {
				DisplayEnemyInfo(target);
				animator.SetBool("IsAttacking", true);
				transform.LookAt(target.transform);
			}
		}

		if (target) {
			agent.stoppingDistance = 1f;
		} else {
			agent.stoppingDistance = 0f;
		}
		if (newPointPanel.activeSelf && stats.points == 0) {
			newPointPanel.SetActive(false);
		}
	}

	void LateUpdate() {
		cam.transform.position = transform.position - (cam.transform.forward * camDistance);
		HandleCameraDistance();
	}

	void HandleMousePosition() {
		RaycastHit hit;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		Debug.DrawLine(ray.origin, ray.GetPoint(100));

		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f, LayerMask.GetMask("Zombie"))) {
			if (hit.transform.CompareTag("Zombie")) {
				DisplayEnemyInfo(hit.transform.GetComponent<Zombie>());
			} 
		} else {
			enemyPanel.SetActive(false);
		}
	}

	void HandleMouseClick() {

		RaycastHit hit;

		if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) {
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f)) {
				if (hit.transform.CompareTag("Zombie")) {
					target = hit.transform.GetComponent<Zombie>();
					if (target.isDying || target.GetHP() <= 0) {
						target = null;
					} else {
						targetLocked = true;
						animator.SetBool("IsRunning", true);
					}
				} else if (hit.transform.CompareTag("Terrain")) {
					targetLocked = false;
				}
			}
		}

		if (Input.GetMouseButton(0) && !targetLocked && !EventSystem.current.IsPointerOverGameObject()) {
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f)) {
				if (hit.transform.CompareTag("Terrain")) {
					agent.destination = hit.point;
					target = null;
					animator.SetBool("IsRunning", true);
					animator.SetBool("IsAttacking", false);
				}
			}
		}

		if (Input.GetMouseButtonUp(0) && targetLocked) {
			lastAttack = true;
		}

	}


	void HandleGameOver() {
		if (gameOver && Input.GetKeyDown(KeyCode.Space)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

	public void HitEnemy() {
		if (!target || agent.remainingDistance > agent.stoppingDistance) return;
		Stats targetStats = target.GetComponent<Stats>();
		int chance = stats.HitChance(targetStats);
		
		if (Random.Range(0, 101) < chance && !target.TakeDamage(stats.DamageValue(targetStats))) {
			UnsetTarget();
			IncreaseXP(targetStats.xp);
		}
		if (lastAttack) {
			UnsetTarget();
		}
	}

	public void OnPlayerDeath() {
		gameOver = true;
	}

	public void OnPlayerEndDeath() {
		gameOverPanel.SetActive(true);
	}

	void UnsetTarget() {
		animator.SetBool("IsAttacking", false);
		target = null;
		targetLocked = false;
		agent.destination = transform.position;
		enemyPanel.SetActive(false);
		lastAttack = false;
	}

	void DisplayEnemyInfo(Zombie z) {
		enemyStats = z.GetStats();
		enemyPanel.SetActive(true);
		enemyHP.value = z.GetHP() / (float)enemyStats.HP;
		enemyNameAndLvl.text = enemyStats.firstName + " - lvl " + enemyStats.level;
	}

	void UpdatePlayerInfo() {
		hpSlider.value = realHP / (float)stats.HP;
		hpText.text = realHP.ToString();
		lvlText.text = stats.level.ToString();
	}

	public bool TakeDamage(int amount) {
		realHP -= amount;
		if (realHP <= 0) {
			animator.SetBool("IsDead", true);
			agent.destination = transform.position;
			agent.ResetPath();
			gameOver = true;
			return false;
		}
		return true;
	}

	void IncreaseXP(int amount) {
		stats.xp += amount;
		if (stats.xp >= requiredXP) {
			stats.level++;
			stats.xp = stats.xp - requiredXP;
			realHP = stats.HP;
			requiredXP = (int)((float)requiredXP * 1.5f);
			stats.points += 5;
			newPointPanel.SetActive(true);
			ptcSystem.Play();
			Invoke("StopParticleSystem", ptcSystem.main.duration);
		}
		xpText.text = stats.xp + " / " + requiredXP;
		xpSlider.value = (stats.xp / (float)requiredXP);
	}

	void StopParticleSystem() {
		ptcSystem.Stop();
	}

	void HandleCameraDistance() {
		return; // TODO
		RaycastHit hit;

		if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, camDistance - 0.1f)) {
			//Debug.DrawLine(cam.transform.position, hit.point, Color.red);
			camDistance -= 0.1f;
		} else {
			camDistance += 0.1f;
		}
		camDistance = Mathf.Clamp(camDistance, 3f, 10f);
	}

	public Stats GetStats() {
		return stats;
	}

	public void Heal() {
		realHP += (float)stats.HP * 0.3f;
		if (realHP > stats.HP) {
			realHP = stats.HP;
		}
	}
}
