﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats : MonoBehaviour {

	public string firstName;
	public int strengh;
	public int agility;
	public int constitution;
	[Range(0, 160)]
	public int armor;
	public int HP;
	public int minDamage;
	public int maxDamage;
	public int level;
	public int xp;
	public int money;

	public int points;


	void Start() {
		ComputeCaracteristics();
	}

	public int HitChance(Stats target) {
		return 75 + agility - target.agility;
	}

	public int DamageValue(Stats target) {
		return Random.Range(minDamage, maxDamage + 1) * (1 - target.armor / 200);
	}

	public void ComputeCaracteristics() {
		HP = 5 * constitution;
		minDamage = strengh / 2;
		maxDamage = minDamage + 4;
	}

	public void IncreaseStrengh() {
		if (points <= 0) return;
		strengh++;
		ComputeCaracteristics();
		points--;
	}

	public void IncreaseAgility() {
		if (points <= 0) return;
		agility++;
		points--;
	}

	public void IncreaseConstitution() {
		if (points <= 0) return;
		constitution++;
		ComputeCaracteristics();
		points--;
	}
}
