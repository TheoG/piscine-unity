﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Caracteristics : MonoBehaviour {

	public GameObject caracteristicsPanel;

	public Text titleT;

	public Text strenghT;
	public Text agilityT;
	public Text constitutionT;
	public Text armorT;
	public Text damagesT;
	public Text maxHPT;
	public Text xpT;
	public Text creditsT;

	public Button strenghB;
	public Button agilityB;
	public Button constitutionB;

	public bool visible;
	public static Caracteristics instance = null;

	private Stats stats;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	void Start() {
		stats = Player.instance.GetStats();
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.C)) {
			SetVisible(!visible);
		}
	}

	public void SetVisible(bool visible) {
		this.visible = visible;
		if (visible) {
			FillData();
		}
		caracteristicsPanel.SetActive(visible);
	}

	public void FillData() {
		titleT.text = stats.name + " - " + stats.level + " (" + stats.points + " points" + ")";

		strenghB.gameObject.SetActive(stats.points > 0);
		agilityB.gameObject.SetActive(stats.points > 0);
		constitutionB.gameObject.SetActive(stats.points > 0);

		strenghT.text = stats.strengh.ToString();
		agilityT.text = stats.agility.ToString();
		constitutionT.text = stats.constitution.ToString();
		armorT.text = stats.armor.ToString();

		damagesT.text = stats.minDamage + " - " + stats.maxDamage;
		maxHPT.text = stats.HP.ToString();
		xpT.text = stats.xp + " / " + Player.instance.requiredXP;
		creditsT.text = stats.money.ToString();
	}
}
