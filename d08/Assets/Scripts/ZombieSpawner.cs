﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {

	public List<GameObject> zombies;

	private GameObject zombieInstance;

	void Start () {

		if (zombies != null) {
			StartCoroutine(SpawnZombieAfterTime(0));
		}

	}

	void OnDisable() {
		if (zombieInstance) {
			zombieInstance.GetComponent<Zombie>().onZombieDie -= SpawnZombie;
		}
	}

	void Update () {
		
	}

	public void SpawnZombie() {
		StartCoroutine(SpawnZombieAfterTime(Random.Range(3f, 8f)));
	}

	IEnumerator SpawnZombieAfterTime(float time) {

		for (int i = 0; i < time * 10; i++) {
			yield return new WaitForSeconds(0.1f);
		}

		int index = Random.Range(0, zombies.Count);

		zombieInstance = Instantiate(zombies[index], transform.position, zombies[index].transform.rotation);
		Zombie zombieScript = zombieInstance.GetComponent<Zombie>();
		zombieScript.onZombieDie += SpawnZombie;
		zombieScript.SetLevel(Player.instance.GetStats().level);
	}
}
