﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine.UI;
using UnityEngine;

public class LevelManager : MonoBehaviour {


	public static LevelManager instance = null;


	public List<PlayerPreferences.Level> levelList;
	public List<Sprite> levelImages;
	public int currentLevel = 0;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	void Start () {
		SetupLevelList();
		DontDestroyOnLoad(this);
	}

	public void SetupLevelList() {
		levelList.Clear();
		var levelInfo = PlayerPreferences.instance.GetLevelInfo();
		Debug.Log("Get fresh data");
		foreach (PlayerPreferences.Level level in levelInfo.levels) {
			levelList.Add(level);
		}		
	}

	public void UnlockNextLevel() {
		if (currentLevel != 3 && currentLevel != 7 && currentLevel != 11) {
			levelList[currentLevel + 1].unlocked = true;
		}
		if (Sonic.instance.score > levelList[currentLevel].bestScore) {
			levelList[currentLevel].bestScore = Sonic.instance.score;
		}

		PlayerPreferences.instance.WriteData();
		Sonic.instance.score = 0;
		Sonic.instance.rings = 0;
		Sonic.instance.nbDied = 0;
	}

	void Update () {
		
	}
}
