﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSignScript : MonoBehaviour {


	public delegate void OnEndSignEnter();
	public static OnEndSignEnter onEndSignEnter;

	public bool rotation = false;

	private void Update() {
		if (rotation) {
			transform.Rotate(Vector3.up, 100000000f * Time.deltaTime);
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.CompareTag("Player")) {
			rotation = true;
			StartCoroutine(StopRotation());
			onEndSignEnter();
		}
	}

	IEnumerator StopRotation() {
		yield return new WaitForSeconds(6.7f);
		rotation = false;
	}
}
