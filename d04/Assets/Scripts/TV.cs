﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV : MonoBehaviour {

	public Sprite brokenSprite;

	public SpriteRenderer spriteRenderer;


	private void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void DestroyTV() {
		spriteRenderer.sprite = brokenSprite;
		Destroy(gameObject, 2);
	}
}
