﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DataSelectScript : MonoBehaviour {

	public Sprite bluredLevel;
	public GameObject tilePrefab;
	public GameObject levelList;
	public List<Sprite> levelImages;
	public List<string> sceneNames;
	public GameObject levelSelector;

	public Text bestScoreText;

	//private Canvas canvas;
	private int x = 0;
	private int y = 0;
	private int tileWidth = 0;
	private int spaceX = 0;
	private int spaceY = 0;
	private Vector3 origin;

	private List<PlayerPreferences.Level> infos;

	void Start () {

		sceneNames.AddRange(PlayerPreferences.SceneTitles);
		infos = LevelManager.instance.levelList;

		var layoutGroup = levelList.GetComponent<GridLayoutGroup>();
		tileWidth = (int)layoutGroup.cellSize.x;
		spaceX = (int)layoutGroup.spacing.x;
		spaceY = (int)layoutGroup.spacing.y;
		Debug.Log(tileWidth);
		origin = levelSelector.transform.localPosition;
		for (int i = 0; i < infos.Count; i++) {
			GameObject tile = Instantiate(tilePrefab);
			tile.transform.SetParent(levelList.transform);
			tile.transform.SetAsLastSibling();
			tile.transform.localScale = Vector3.one;
			tile.transform.GetChild(0).GetComponent<Image>().sprite = infos[i].unlocked ? levelImages[i] : bluredLevel;
			tile.transform.GetChild(1).GetComponent<Text>().text = PlayerPreferences.LevelTitles[i];
		}

	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			x++;
			if (x > 3) x = 0;
			Vector3 position = origin;
			position.x += (tileWidth + spaceX) * x;
			position.y -= (tileWidth + spaceY) * y;
			levelSelector.transform.localPosition = position;
			UpdateBestScoreText();
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			x--;
			if (x < 0) x = 3;
			Vector3 position = origin;
			position.x += (tileWidth + spaceX) * x;
			position.y -= (tileWidth + spaceY) * y;
			levelSelector.transform.localPosition = position;
			UpdateBestScoreText();
		}
		if (Input.GetKeyDown(KeyCode.UpArrow)) {
			y--;
			if (y < 0) y = 2;
			Vector3 position = origin;
			position.x += (tileWidth + spaceX) * x;
			position.y -= (tileWidth + spaceY) * y;
			levelSelector.transform.localPosition = position;
			UpdateBestScoreText();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow)) {
			y++;
			if (y > 2) y = 0;
			Vector3 position = origin;
			position.x += (tileWidth + spaceX) * x;
			position.y -= (tileWidth + spaceY) * y;
			levelSelector.transform.localPosition = position;
			UpdateBestScoreText();
		}


		if (Input.GetKeyDown(KeyCode.Return)) {
			if (infos[Index()].unlocked) {
				SceneManager.LoadScene(sceneNames[Index()]);
				if (LevelManager.instance) {
					LevelManager.instance.currentLevel = Index();
				}
			}

		}
	}

	void UpdateBestScoreText() {
		bestScoreText.text = "Best Score:\n" + infos[Index()].bestScore;
	}

	int Index() {
		return y * 4 + x; 
	}
}
