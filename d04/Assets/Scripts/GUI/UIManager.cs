﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UIManager : MonoBehaviour {


	public Text time;
	public Text ringsText;
	public Text scoreText;
	[Header("Level End")]
	public GameObject finalScorePanel;
	public Text finalScoreText;
	public bool end = false;

	void Update () {
		UpdateTime();
		UpdateScore();
		if (end && Input.anyKeyDown) {
			LevelManager.instance.UnlockNextLevel();
			SceneManager.LoadScene(1);
		}
	}

	private void OnEnable() {
		Sonic.onGetRing += UpdateRings;
		EndSignScript.onEndSignEnter += DisplayScore;
	}

	private void OnDisable() {
		Sonic.onGetRing -= UpdateRings;
		EndSignScript.onEndSignEnter -= DisplayScore;
	}

	void UpdateTime() {
		if (end) return;
		string minutes = Mathf.Floor((int)Time.timeSinceLevelLoad / 60).ToString("00");
		string seconds = ((int)Time.timeSinceLevelLoad % 60).ToString("00");
		time.text = minutes + ":" + seconds;
	}

	void UpdateRings(int rings) {
		ringsText.text = rings.ToString();
	}

	void UpdateScore() {
		if (end) return;
		scoreText.text = Sonic.instance.ComputePlayScore().ToString();
	}

	void DisplayScore() {
		if (end) return;
		StartCoroutine(DisplayScoreCoroutine());
	}

	IEnumerator DisplayScoreCoroutine() {
		yield return new WaitForSeconds(6.7f);
		finalScorePanel.SetActive(true);
		if (!end) {
			finalScoreText.text = "Score: " + Sonic.instance.ComputeScore().ToString();
		}
		end = true;
	}
}
