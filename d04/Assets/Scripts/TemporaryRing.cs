﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryRing : MonoBehaviour {

	void Start() {
		Invoke("SetPickable", 2);
		GetComponent<Rigidbody2D>().AddForce(new Vector2(
			Random.Range(-10f, 10f),
			Random.Range(1f, 10f)), ForceMode2D.Impulse);
	}

	void SetPickable() {
		gameObject.layer = LayerMask.NameToLayer("ring");
		StartCoroutine(blinking(4));
	}

	IEnumerator blinking(int time) {
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		for (int i = 0; i < time / 0.2f; i++) {
			sr.color = Color.clear;
			yield return new WaitForSeconds(0.1f);
			sr.color = Color.white;
			yield return new WaitForSeconds(0.1f);
		}
		Destroy(gameObject);
	}
}
