﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPreferences : MonoBehaviour {

	private const string TOTAL_LIFES_FIELD = "life";
	private const string TOTAL_RINGS_FIELD = "nbRings";
	private const string MAPS_FIELD = "levelInfo";

	public static PlayerPreferences instance = null;


	private void Start() {
		if (!instance) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad(this);
	}

	public int GetTotalLife() {
		int totalLife = PlayerPrefs.GetInt(PlayerPreferences.TOTAL_LIFES_FIELD, -1);
		if (totalLife < 0) {
			SetTotalLife(0);
			return 0;
		}
		return totalLife;
	}

	public int GetTotalRings() {
		int totalRings = PlayerPrefs.GetInt(PlayerPreferences.TOTAL_RINGS_FIELD, -1);
		if (totalRings < 0) {
			SetTotalRings(0);
			return 0;
		}
		return totalRings;
	}

	public LevelList GetLevelInfo() {
	
		string levelInfo = PlayerPrefs.GetString(PlayerPreferences.MAPS_FIELD, "null");
		if (levelInfo.Equals("null")) {
			CreateLevelField();
			return GetLevelInfo();
		}

		LevelListJson levelListJson = JsonUtility.FromJson<LevelListJson>(levelInfo);
		LevelList levelList = new LevelList();
		levelList.levels = new List<Level>();
		foreach(string levelJson in levelListJson.levels) {
			levelList.levels.Add(JsonUtility.FromJson<Level>(levelJson));
		}
		return levelList;
	} 

	public void SetTotalRings(int rings) {
		PlayerPrefs.SetInt(PlayerPreferences.TOTAL_RINGS_FIELD, rings);
	}

	public void SetTotalLife(int life) {
		PlayerPrefs.SetInt(PlayerPreferences.TOTAL_LIFES_FIELD, life);
	}

	public void CreateLevelField() {
		List<string> levelString = new List<string>();
		for (int i = 0; i < LevelTitles.Length; i++) {
			Level level = new Level();
			level.id = i;
			level.name = LevelTitles[i];
			level.bestScore = 0;
			level.unlocked = i % 4 == 0;
			string json = JsonUtility.ToJson(level);
			levelString.Add(json);
		}
		LevelListJson levelList = new LevelListJson();
		levelList.levels = levelString;
		PlayerPrefs.SetString(PlayerPreferences.MAPS_FIELD, JsonUtility.ToJson(levelList));
	}

	private void SetLevelInfo() {
		List<string> levelString = new List<string>();
		for (int i = 0; i < LevelManager.instance.levelList.Count; i++) {
			string json = JsonUtility.ToJson(LevelManager.instance.levelList[i]);
			levelString.Add(json);
		}
		LevelListJson levelList = new LevelListJson();
		levelList.levels = levelString;
		PlayerPrefs.SetString(PlayerPreferences.MAPS_FIELD, JsonUtility.ToJson(levelList));
	}

	public void Reset() {
		PlayerPrefs.DeleteAll();
		SetTotalLife(0);
		SetTotalRings(0);
		CreateLevelField();
	}

	public void WriteData() {
		int rings = GetTotalRings();
		int lifes = GetTotalLife();
		SetTotalRings(rings + Sonic.instance.rings);
		SetTotalLife(lifes + Sonic.instance.nbDied);
		SetLevelInfo();
	}

	public static readonly string[] LevelTitles = {
		"Green Hill", "Labyrinth", "Spring Yard", "Desert",
		"Green Hill", "Labyrinth", "Spring Yard", "Desert",
		"Green Hill", "Labyrinth", "Spring Yard", "Desert",
	};

	public static readonly string[] SceneTitles = {
		"ModernRoute_GH", "ModernRoute_L", "ModernRoute_SY", "ModernRoute_D",
		"MetalRoute_GH", "MetalRoute_L", "MetalRoute_SY", "MetalRoute_D",
		"DubstepRoute_GH", "DubstepRoute_L", "DubstepRoute_Y", "DubstepRoute_D",
	};

	[SerializeField]
	public class LevelListJson {
		public List<string> levels;
	}

	public class LevelList {
		public List<Level> levels;
	}

	[SerializeField]
	[System.Serializable]
	public class Level {
		public int id;
		public string name;
		public bool unlocked;
		public int bestScore;
	}

}
