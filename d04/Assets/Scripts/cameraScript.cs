﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {

	public GameObject activePlayer;

	public AudioClip mainMusic;
	public AudioClip endMusic;

	public AudioSource musicAudioSource;

	void Start () {
		putCamera(activePlayer);

	}

	//Camera follow player
	void putCamera(GameObject player) {
		gameObject.transform.SetParent (player.transform);
		gameObject.transform.localPosition  = new Vector3(0, 1.5f, -10);
	}

	public void StartEndMusic() {
		musicAudioSource.Pause();
		musicAudioSource.clip = endMusic;
		musicAudioSource.Play();
	}
}
