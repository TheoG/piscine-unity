﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumber : MonoBehaviour {


	public Sprite off;
	public Sprite on;

	private SpriteRenderer spriteRenderer;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {

		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		audioSource = gameObject.GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private void OnTriggerEnter2D(Collider2D collision) {
		spriteRenderer.sprite = on;
		audioSource.Play();
		Invoke("ResetBumper", 0.3f);
	}

	void ResetBumper() {
		spriteRenderer.sprite = off;
	}
}
