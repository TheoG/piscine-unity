﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public GameObject ballCamera;
	public GameObject freeCamera;

	public Vector3 ballCameraOffset;

	private bool freeMotionCamera = false;
	private UnityStandardAssets.Cameras.FreeLookCam freeCameraScript;

	public static CameraManager instance = null;

	void Awake() {
	
		if (!instance) {
			instance = this;
		}

	}

	void Start () {
		freeCamera.SetActive(false);
		ballCamera.SetActive(true);
		freeCameraScript = freeCamera.GetComponent<UnityStandardAssets.Cameras.FreeLookCam>();

		//ballCamera.transform.rotation
	}

	void Update () {
		//ballCamera.transform.position = Ball.instance.transform.position - ballCameraOffset;

		if (freeCamera.activeSelf) {
			freeCamera.transform.Translate(new Vector3(Input.GetAxis("Vertical"), Input.GetAxis("Height"), -Input.GetAxis("Horizontal")));
			Vector3 clampedPosition = freeCamera.transform.position;
			clampedPosition.x = Mathf.Clamp(clampedPosition.x, 70, 400);
			clampedPosition.y = Mathf.Clamp(clampedPosition.y, 4, 50);
			clampedPosition.z = Mathf.Clamp(clampedPosition.z, 80, 400);
			freeCamera.transform.position = clampedPosition;
		}

		if (Input.GetKeyDown(KeyCode.C)) {
			freeMotionCamera = !freeMotionCamera;
			freeCameraScript.mouseMovement = freeMotionCamera;
			freeCamera.SetActive(freeMotionCamera);
			ballCamera.SetActive(!freeMotionCamera);
			Ball.instance.canRotate = !freeMotionCamera;
		}
	}
}
