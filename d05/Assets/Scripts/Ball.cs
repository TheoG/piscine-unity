﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

	public float power;
	public bool selectPower = false;
	public Slider powerSlider;

	public float rotationSpeed = 10;
	public GameObject arrow;
	public bool canRotate = true;
	public bool canShoot = true;

	public static Ball instance = null;
	private Rigidbody rb;
	private float startTime = 0f;
	private Vector3 forceDirection;

	private int clubIndex = 0;
	private Club currentClub = null;
	private SphereCollider sphereCollider;
	private bool isOnGreen = false;
	private bool isOnSand = false;
	private Vector3 lastPos;

	public List<Club> clubs;

	public PhysicMaterial defaultMaterial;
	public PhysicMaterial sandMaterial;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	void Start () {
		rb = GetComponent<Rigidbody>();
		sphereCollider = GetComponent<SphereCollider>();
		arrow.transform.position = CameraManager.instance.ballCamera.transform.forward * 4f + CameraManager.instance.ballCamera.transform.up * 2f;
		InitClubs();
		ChangeClub(0);
		ResetBallOrientation(0);
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) && canShoot != false) {
			if (selectPower) {
				lastPos = transform.position;
				forceDirection = CameraManager.instance.ballCamera.transform.TransformDirection(Vector3.forward);
				forceDirection *= currentClub.forceZ * power;
				forceDirection.y = currentClub.forceY * power;
				rb.AddForce(forceDirection);
				rb.AddRelativeTorque(0f, 0f, 100f * power);
				MapManager.instance.nbShots[MapManager.instance.currentHole]++;
				selectPower = false;
				canShoot = false;

			} else {
				startTime = 0;
				selectPower = true;
			}
			arrow.SetActive(selectPower);
		}
		Vector3 offset = Vector3.right * 10f;
		offset.y = -3f;
		CameraManager.instance.ballCamera.transform.position = transform.position - (CameraManager.instance.ballCamera.transform.forward * 4f) + (CameraManager.instance.ballCamera.transform.up * 2f);
		arrow.transform.position = transform.position + (CameraManager.instance.ballCamera.transform.forward * 4f) + (CameraManager.instance.ballCamera.transform.up * 2f);
		if (canShoot && canRotate) {
			if (Input.GetKey(KeyCode.A)) {
				transform.Rotate(Vector3.up, -10f * rotationSpeed * Time.deltaTime);
				arrow.transform.RotateAround(transform.position, Vector3.up, -10f * rotationSpeed * Time.deltaTime);
				CameraManager.instance.ballCamera.transform.RotateAround(transform.position, Vector3.up, -10f * rotationSpeed * Time.deltaTime);
			} else if (Input.GetKey(KeyCode.D)) {
				transform.Rotate(Vector3.up, 10f * rotationSpeed * Time.deltaTime);
				arrow.transform.RotateAround(transform.position, Vector3.up, 10f * rotationSpeed * Time.deltaTime);
				CameraManager.instance.ballCamera.transform.RotateAround(transform.position, Vector3.up, 10f * rotationSpeed * Time.deltaTime);
			}
		}

		if (selectPower) {
			startTime += Time.deltaTime;
			power = Mathf.PingPong(startTime / 2.0f, 1);
			powerSlider.value = power;
		}

		if (Input.GetKeyDown(KeyCode.R)) {
			MapManager.instance.ResetCurrentHole();
		}
		if (transform.position.y < -20) {
			MapManager.instance.ResetCurrentHole();
		}
		HandleClub();
	}

	private void HandleClub() {
		if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
			clubIndex = (clubIndex + 1) % clubs.Count;
			if (isOnGreen) {
				clubIndex = 3;
			}
			if (isOnSand) {
				clubIndex = 2;
			}
			ChangeClub(clubIndex);
			if (!isOnGreen && currentClub.cName.Equals("Putter")) {
				clubIndex = (clubIndex + 1) % clubs.Count;
				ChangeClub(clubIndex);
			}
		}
		if (Input.GetKeyDown(KeyCode.Alpha1) && !isOnGreen && !isOnSand) {
			clubIndex = 0;
			ChangeClub(clubIndex);
		} else if (Input.GetKeyDown(KeyCode.Alpha2) && !isOnGreen && !isOnSand) {
			clubIndex = 1;
			ChangeClub(clubIndex);
		} else if (Input.GetKeyDown(KeyCode.Alpha3) && !isOnGreen) {
			clubIndex = 2;
			ChangeClub(clubIndex);
		} else if (Input.GetKeyDown(KeyCode.Alpha4) && isOnGreen && !isOnSand) {
			clubIndex = 3;
			ChangeClub(clubIndex);
		}
	}

	public void ChangeClub(int index) {
		currentClub = clubs[index];
		MapManager.instance.DisplayClubName(currentClub.cName);
	}

	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.CompareTag("Ground")) {
			rb.drag = 0.9f;
			rb.angularDrag = 10f;
		}
	}

	private void OnCollisionStay(Collision collision) {
		if (collision.gameObject.CompareTag("Ground")) {
			if (rb.velocity.magnitude < 0.01f && !canShoot) {
				canShoot = true;
				arrow.SetActive(true);
				ResetBallOrientation(MapManager.instance.currentHole);
			}
		}
	}

	private void OnCollisionExit(Collision collision) {
		if (collision.gameObject.CompareTag("Ground")) {
			rb.drag = 0f;
			rb.angularDrag = 0f;
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Hole")) {
			MapManager.instance.CheckRightHole(other);
		}
		if (other.CompareTag("Sand")) {
			sphereCollider.material = sandMaterial;
			ChangeClub(2);
			isOnSand = true;
		}
		if (other.CompareTag("Green")) {
			ChangeClub(3);
			isOnGreen = true;
		}
		if (other.CompareTag("Water")) {
			transform.position = lastPos;
			rb.velocity = Vector3.zero;
			MapManager.instance.nbShots[MapManager.instance.currentHole]++;
			ResetBallOrientation(MapManager.instance.currentHole);
			MapManager.instance.DisplayLoserPanel();
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Sand")) {
			sphereCollider.material = defaultMaterial;
		}
		if (other.CompareTag("Green")) {
			ChangeClub(0);
			isOnGreen = false;
		}
		if (other.CompareTag("Sand")) {
			isOnSand = false;
		}
	}

	public void ResetBallOrientation(int index) {
		
		transform.LookAt(MapManager.instance.ends[index].transform.position);

		arrow.transform.LookAt(2 * transform.position - MapManager.instance.ends[index].transform.position);

		Vector3 relativePos = MapManager.instance.ends[index].transform.position - transform.position;
		Vector3 arrowRelativePos = transform.position - MapManager.instance.ends[index].transform.position;

		// the second argument, upwards, defaults to Vector3.up
		Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
		Quaternion arrowRotation = Quaternion.LookRotation(arrowRelativePos, Vector3.up);
		CameraManager.instance.ballCamera.transform.rotation = rotation;

		arrow.transform.rotation = arrowRotation;
	}

	void InitClubs() {

		clubs = new List<Club>();

		Club wood = new Club("Wood", 30f, 100f);
		Club iron = new Club("Iron", 100f, 50f);
		Club wegde = new Club("Wedge", 50f, 30f);
		Club putter = new Club("Putter", 5f, 40f);

		clubs.Add(wood);
		clubs.Add(iron);
		clubs.Add(wegde);
		clubs.Add(putter);
	}

	public class Club {

		public string cName;
		public float forceY;
		public float forceZ;

		public Club(string name, float fY, float fZ) {
			cName = name;
			forceY = fY;
			forceZ = fZ;
		}
	}

}
