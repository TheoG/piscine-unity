﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MapManager : MonoBehaviour {


	public List<GameObject> begins;
	public List<Collider> ends;
	public List<int> par;
	public int currentHole;
	public int[] nbShots;

	[Header("GUI")]
	public Text currentHoleText;
	public Text currentHoleParText;
	public Text currentHoleShotText;
	public Text currentClub;

	public GameObject scorePanel;
	public GameObject summaryPanel;
	public GameObject loserPanel;

	public Text scoreText;
	public Text finalScoreText;
	public Text loserTimerText;
	

	public static MapManager instance = null;

	private bool inHole = false;
	private bool inWrongHole = false;

	public static readonly string[] Titles = { "Ace", "Albatros", "Eagle", "Birdie", "Par", "Bogey", "Double Bogey", "Triple Bogey" };

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		currentHole = 0;
		nbShots = new int[begins.Count];
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)) {
			summaryPanel.SetActive(true);
			FillSummaryValues();
		} else if (Input.GetKeyUp(KeyCode.Tab)) {
			summaryPanel.SetActive(false);
		}
	}

	void FixedUpdate() {


		if (Input.GetKeyDown(KeyCode.Return)) {
			if (inHole) {
				currentHole = (currentHole + 1) % begins.Count;
				inHole = false;
				scorePanel.SetActive(false);
				ResetCurrentHole();
				Ball.instance.canShoot = true;
				Ball.instance.ChangeClub(0);
				if (summaryPanel.activeSelf) {
					ResetGame();
					summaryPanel.SetActive(false);
				}
			}
			if (inWrongHole) {
				inWrongHole = false;
				ResetCurrentHole();
			}
		}

		currentHoleText.text = "Current Hole: " + currentHole;
		currentHoleParText.text = "Par: " + par[currentHole];
		currentHoleShotText.text = "Shots: " + nbShots[currentHole];
	}

	public void CheckRightHole(Collider collider) {
		if (ends[currentHole] == collider) {
			inHole = true;
			if (currentHole == begins.Count - 1) {
				DisplayFinalScore();
			} else {
				DisplayScore();
			}
			Ball.instance.canShoot = false;
		} else {
			inWrongHole = true;
		}

	}

	public void ResetCurrentHole() {
		Ball.instance.transform.position = begins[currentHole].transform.position;
		Rigidbody rb = Ball.instance.GetComponent<Rigidbody>();
		rb.velocity = Vector3.zero;
		rb.rotation = Quaternion.identity;
		Ball.instance.ResetBallOrientation(currentHole);
		nbShots[currentHole] = 0;
	}

	public void FillSummaryValues() {
		Transform scores = summaryPanel.transform.GetChild(1);

		for (int i = 0; i < nbShots.Length; i++) {
			Transform line = scores.GetChild(i);
			line.GetChild(2).GetComponent<Text>().text = nbShots[i].ToString();
			line.GetChild(4).GetComponent<Text>().text = par[i].ToString();
		}
	}

	private void DisplayScore() {
		scorePanel.SetActive(true);
		scoreText.text = GetScoreScore(nbShots[currentHole], par[currentHole]);
	}

	public void DisplayClubName(string clubName) {
		currentClub.text = clubName;
	}

	private void DisplayFinalScore() {
		FillSummaryValues();
		summaryPanel.SetActive(true);
		finalScoreText.gameObject.SetActive(true);
		int totalScore = 0;
		int totalPar = 0;

		for (int i = 0; i < nbShots.Length; i++) {
			totalScore += nbShots[i];
			totalPar += par[i];
		}

		finalScoreText.text = GetScoreScore(totalScore, totalPar);
	}

	public void DisplayLoserPanel() {
		loserPanel.SetActive(true);
		StartCoroutine(LoserPanelCoroutine(3));
	}

	private void ResetGame() {
		for (int i = 0; i < nbShots.Length; i++) {
			nbShots[i] = 0;
		}
	}
	public string GetScoreScore(int score, int par) {
		if (nbShots[currentHole] == 1) return "Ace";
		if (nbShots[currentHole] - par <= -3) return "Albatross";
		if (nbShots[currentHole] - par == -2) return "Eagle";
		if (nbShots[currentHole] - par == -1) return "Birdie";
		if (nbShots[currentHole] - par == 0) return "Par";
		if (nbShots[currentHole] - par == 1) return "Bogey";
		if (nbShots[currentHole] - par == 2) return "Double Bogey";
		if (nbShots[currentHole] - par == 3) return "Triple Bogey";
		return "+" + nbShots[currentHole];
	}

	IEnumerator LoserPanelCoroutine(int time) {
		for (int i = 0; i <= time; i++) {
			yield return new WaitForSeconds(1);
			loserTimerText.text = (time - i).ToString();
		}
		loserPanel.SetActive(false);
	}
}
