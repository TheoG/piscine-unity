﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : Activable {

	public GameObject securityCamera;
	public GameObject lightBulb;

	private bool on = true;

	private Vector3 onRotation;
	private Vector3 offRotation;

	private void Start() {
		onRotation = new Vector3(90f, 0f, 0f);
		offRotation = new Vector3(270f, -90f, -90f);
	}

	protected override void ActivableAction() {

		on = !on;

		securityCamera.SetActive(on);
		lightBulb.SetActive(on);

		transform.rotation = on ? Quaternion.Euler(onRotation.x, onRotation.y, onRotation.z) 
			: Quaternion.Euler(offRotation.x, offRotation.y, offRotation.z);
	}

}
