﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : Activable {

	public GameObject cameraCollider;

	private ParticleSystem smokeParticle;
	private bool isEnabled = false;
	
	private void Start() {
		smokeParticle = GetComponentInChildren<ParticleSystem>();
	}

	protected override void ActivableAction() {
		isEnabled = !isEnabled;

		cameraCollider.SetActive(!isEnabled);
		if (isEnabled) {
			Debug.Log("Start it");
			smokeParticle.Play();
		} else {
			Debug.Log("Stop it");
			smokeParticle.Stop();
		}
	}
}
