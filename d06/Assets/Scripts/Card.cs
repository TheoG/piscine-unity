﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : Activable {

	protected override void ActivableAction() {
		PlayerController.instance.hasKey = true;
		transform.GetChild(0).gameObject.SetActive(false);
		helper = "";
		SoundManager.instance.PlayKeyPickUp();
	}
}
