﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance = null;
	public AudioClip keySound;
	public AudioClip accessGranted;
	public AudioClip accessDenied;

	private AudioSource audioSource;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	private void Start() {
		audioSource = GetComponent<AudioSource>();
	}

	public void PlayKeyPickUp() {
		StopSound();
		audioSource.clip = keySound;
		PlaySound();
	}

	public void PlayAccessGranted() {
		StopSound();
		audioSource.clip = accessGranted;
		PlaySound();
	}

	public void PlayAccessDenied() {
		StopSound();
		audioSource.clip = accessDenied;
		PlaySound();
	}

	private void StopSound() {
		if (audioSource.isPlaying) {
			audioSource.Stop();
		}
	}

	private void PlaySound() {
		audioSource.Play();
	}
}
