﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Activable : MonoBehaviour {

	public string helper;

	protected void DisplayMessage() {
		GameManager.instance.SetGUIMessage(helper);
	}

	protected void HideMessage() {
		GameManager.instance.HideGUIMessage();
	}


	protected virtual void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {
			DisplayMessage();
		}
	}

	protected virtual void OnTriggerStay(Collider other) {
		if (other.CompareTag("Player")) {
			if (Input.GetKeyDown(KeyCode.E)) {
				ActivableAction();
			}
		}
	}

	protected virtual void OnTriggerExit(Collider other) {
		if (other.CompareTag("Player")) {
			HideMessage();
		}
	}

	protected abstract void ActivableAction();
}
