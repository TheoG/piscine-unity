﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : Activable {

	public Material unlockedMaterial;
	public MeshRenderer meshRenderer;
	public GameObject slideDoor;

	private bool isOpening = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isOpening && slideDoor.transform.position.z >= 18f) {
			slideDoor.transform.Translate(0f, 0f, -Time.deltaTime * 1f, Space.World);
		}
	}

	protected override void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {
			if (PlayerController.instance.hasKey) {
				helper = "Press E to open the door";
			} else {
				helper = "You need to find the key";
			}
		}
		base.OnTriggerEnter(other);
	}
	
	protected override void ActivableAction() {
		if (PlayerController.instance.HasKey()) {
			meshRenderer.material = unlockedMaterial;
			isOpening = true;
			SoundManager.instance.PlayAccessGranted();
		}
		else if (!PlayerController.instance.HasKey()) {
			SoundManager.instance.PlayAccessDenied();
		}
	}
}
