﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {

	public Slider discretionSlider;
	[Range(0.01f, 1f)]
	public float difficulty;
	public GameObject alarms;
	public AudioSource[] alarmsAudioSources;
	public AudioSource calmMusic;
	public AudioSource panicMusic;

	public Text helperText;

	public GameObject alarmLight;

	public static GameManager instance = null;

	private Coroutine startCoroutine;
	private Coroutine blinkingLightCoroutine;

	void Start () {
		if (!instance) {
			instance = this;
		}
		alarmsAudioSources = alarms.GetComponentsInChildren<AudioSource>();

		startCoroutine = StartCoroutine(Instructions());
	}
	
	void Update () {

		if (discretionSlider.value >= 0.75f) {
			foreach(AudioSource source in alarmsAudioSources) {
				if (!source.isPlaying) {
					source.Play();
				}
			}
			if (!panicMusic.isPlaying) {
				blinkingLightCoroutine = StartCoroutine(BlinkingAlarmLight());
				calmMusic.Stop();
				panicMusic.Play();
			}
		} else {
			foreach (AudioSource source in alarmsAudioSources) {
				if (source.isPlaying) {
					source.Stop();
				}
			}
			if (!calmMusic.isPlaying) {
				if (blinkingLightCoroutine != null) {
					alarmLight.SetActive(false);
					StopCoroutine(blinkingLightCoroutine);
				}
				calmMusic.Play();
				panicMusic.Stop();	
			}
		}
		if (discretionSlider.value >= 1f) {
			SetGUIMessage("Simulation Over... You need to be more SPLINTER CELLISH");
			Invoke("ReloadLevel", 3);
		}
		discretionSlider.value -= Time.deltaTime * (difficulty / 8);
		discretionSlider.value = Mathf.Clamp(discretionSlider.value, 0f, 1f);

		if (startCoroutine != null && Input.GetKeyDown(KeyCode.Return)) {
			StopCoroutine(startCoroutine);
			PlayerController.instance.StartToPlay();
			HideGUIMessage();
		} 
	}

	void OnRunStep() {
		if (Input.GetKey(KeyCode.LeftShift)) {
			discretionSlider.value += Time.deltaTime * difficulty * 35;
		}
	}

	private void OnTriggerStay(Collider other) {
		
	}

	private void OnEnable() {
		FirstPersonController.onRunSound += OnRunStep;
	}
	
	private void OnDisable() {
		FirstPersonController.onRunSound -= OnRunStep;
	}

	public void ReloadLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void SetGUIMessage(string helperMessage) {
		helperText.transform.parent.gameObject.SetActive(true);
		helperText.text = helperMessage;
		StartCoroutine(FadeIn(1f));
	}

	public void HideGUIMessage() {
		StartCoroutine(FadeOut(1f));
	}
	
	IEnumerator FadeIn(float time) {
		Color color = Color.grey;
		color.a = 0;
		float step = 1 / (time * 60f);
		for (int i = 0; i < time * 60f; i++) {
			color.a += step;
			helperText.color = color;
			yield return new WaitForSeconds(0.01f);
		}
	}

	IEnumerator FadeOut(float time) {
		Color color = Color.grey;
		color.a = 1;
		float step = 1 / (time * 60f);
		for (int i = 0; i < time * 60f; i++) {
			color.a -= step;
			helperText.color = color;
			yield return new WaitForSeconds(0.01f);
		}
		helperText.transform.parent.gameObject.SetActive(false);
	}

	IEnumerator BlinkingAlarmLight() {
		while (true) {
			alarmLight.SetActive(true);
			yield return new WaitForSeconds(1);
			alarmLight.SetActive(false);
			yield return new WaitForSeconds(1);
		}
	}

	IEnumerator Instructions() {
		SetGUIMessage("Welcome to your training simulation Agent 42.");
		yield return new WaitForSeconds(3f);
		HideGUIMessage();
		yield return new WaitForSeconds(1f);
		SetGUIMessage("Today, you need to find the SUPER EXTRA TOP SECRET FILES.");
		yield return new WaitForSeconds(3f);
		HideGUIMessage();
		yield return new WaitForSeconds(1f);
		SetGUIMessage("Good luck.");
		PlayerController.instance.StartToPlay();
		yield return new WaitForSeconds(1f);
		HideGUIMessage();
	}
}
