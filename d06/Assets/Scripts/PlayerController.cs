﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private float timeInCameraView = 0f;
	public bool hasKey = false;

	public static PlayerController instance = null;

	private void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	private void OnTriggerStay(Collider other) {
		if (other.CompareTag("Camera")) {
			GameManager.instance.discretionSlider.value += GameManager.instance.difficulty * (timeInCameraView / 10);
			timeInCameraView += Time.deltaTime;
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Camera")) {
			timeInCameraView = 0f;
		}
	}

	public bool HasKey() {
		return hasKey;
	}

	public void StartToPlay() {
		UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
		controller.m_WalkSpeed = 5;
		controller.m_RunSpeed = 10;
	}
}
