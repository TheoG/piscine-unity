﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour {

	public Weapon[] availableWeapons;
	public Weapon currentWeapon;
	public Text life;

	public static Player instance = null;

	private int maxHP = 10;
	private int hp;

	private void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	void Start () {
		hp = maxHP;
	}
	
	void Update () {

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			currentWeapon = availableWeapons[0];
			availableWeapons[0].gameObject.SetActive(true);
			availableWeapons[1].gameObject.SetActive(false);
			availableWeapons[1].canShoot = true;
		} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			currentWeapon = availableWeapons[1];
			availableWeapons[0].gameObject.SetActive(false);
			availableWeapons[0].canShoot = true;
			availableWeapons[1].gameObject.SetActive(true);
		}

		life.text = Mathf.Floor((hp / (float)maxHP) * 100).ToString();
		HandleShoot();
	}

	void HandleShoot() {
		if (Input.GetMouseButton(0)) {
			if (currentWeapon) {
				currentWeapon.Shoot();
			}
		}
	}

	void OnParticleCollision(GameObject other) {
		Debug.Log("Pouet in player");
	}

	public void TakeDamage(int amount) {
		hp -= amount;
		Debug.Log("HP: " + hp);
		if (hp <= 0) {
			Time.timeScale = 0;
			Debug.Log("You died");
		}
	}
}
