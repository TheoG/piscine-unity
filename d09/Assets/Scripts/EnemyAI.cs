﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

	public GameObject mapCenter;

	private NavMeshAgent agent;
	private int hp;

	private Coroutine attackCoroutine = null;
	private bool isTargetPlayer = false;
	private Animator animator;

	void Start () {

		hp = 3;
			
		agent = GetComponent<NavMeshAgent>();
		agent.destination = TerrainManager.instance.center.transform.position;
		agent.stoppingDistance = 1.5f;
		animator = GetComponent<Animator>();
		animator.SetBool("IsRunning", true);
		
	}
	
	void Update () {

		if (isTargetPlayer && agent.remainingDistance <= 1.6f && attackCoroutine == null) {
			attackCoroutine = StartCoroutine(DamagePlayer());
			animator.SetBool("IsAttacking", true);
			animator.SetBool("IsRunning", false);
		} else {
			animator.SetBool("IsAttacking", false);
		}

	}

	private void OnTriggerStay(Collider other) {
		if (other.CompareTag("Player") && agent.isActiveAndEnabled) {
			agent.destination = other.transform.position;
			isTargetPlayer = true;
		}
	}

	private void OnCollisionExit(Collision collision) {
		if (collision.transform.CompareTag("Player")) {
			agent.destination = collision.transform.position;
			isTargetPlayer = false;
		}
	}

	public bool TakeDamage(int amount) {
		hp -= amount;

		if (hp <= 0) {
			return false;
		}
		return true;
	}

	IEnumerator DamagePlayer() {
		while (isTargetPlayer) {
			animator.SetBool("IsAttacking", true);
			yield return new WaitForSeconds(1f);
			if (agent.remainingDistance > 2.5f) {
				attackCoroutine = null;
				animator.SetBool("IsAttacking", false);
				yield break;
			} else {
				Player.instance.TakeDamage(1);
			}
		}
	}
}
