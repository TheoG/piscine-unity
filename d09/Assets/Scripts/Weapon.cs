﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	public float cooldown;
	public float damage;
	public bool canShoot = true;

	public enum Type {
		Gun,
		Range
	}

	public ParticleSystem shootParticle;

	public Type type = Type.Gun;

	public AudioSource shootSoundEffect;
	private float angle = 0;
	private float numberParticleEmit = 1;

	private void Start() {
		if (type == Type.Range) {
			SetRange(5, 25);
		} else {
			SetRange(0, 1);
		}
	}

	void SetRange(float newAngle, float newNumberParticleEmit) {
		this.angle = newAngle;
		this.numberParticleEmit = newNumberParticleEmit;
	}


	public void Shoot() {
		if (!canShoot) {
			Debug.Log("Sorry, can't shoot");
			return;
		}

		Debug.Log("Piou !");

		ParticleSystem.ShapeModule psShape = shootParticle.shape;
		psShape.angle = angle;

		StartCoroutine(ShootCooldown());
	}

	IEnumerator ShootCooldown() {
		canShoot = false;
		shootParticle.Emit((int)numberParticleEmit);
		shootSoundEffect.Play();
		yield return new WaitForSeconds(cooldown);
		canShoot = true;
	}

}
