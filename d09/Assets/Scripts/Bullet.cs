﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private List<ParticleCollisionEvent> collisionEvents;
	public ParticleSystem impactPointParticle;
	public ParticleSystem particleLauncher;

	private void Start() {
		collisionEvents = new List<ParticleCollisionEvent>();
	}

	private void OnParticleCollision(GameObject other) {

		ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);

		for (int i = 0; i < collisionEvents.Count; i++) {
			impactPointParticle.transform.position = collisionEvents[i].intersection;
			impactPointParticle.Emit(100);
			//impactPointParticle.Play();
			Debug.Log(collisionEvents[i].intersection);

			if (other.CompareTag("Enemy")) {
				//Destroy(gameObject);
				EnemyAI enemyAI = collisionEvents[i].colliderComponent.GetComponent<EnemyAI>();

				if (!enemyAI.TakeDamage(1)) {
					Destroy(enemyAI.gameObject);
				}
			}
		}
	}
}
